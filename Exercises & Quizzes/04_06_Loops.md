## Loops

### Exercise List:

1. **Create a half pyramid of numbers**
   Create a half pyramid of numbers (like the below example) by using loops in HyperDbg.

1 

1 2 

1 2 3 

1 2 3 4 

1 2 3 4 5

### Links:

* [? (evaluate and execute expressions and scripts in debuggee)](https://docs.hyperdbg.org/commands/debugging-commands/eval)

* [Conditionals & Loops](https://docs.hyperdbg.org/commands/scripting-language/conditionals-and-loops)
