## Disassembling Code

### Exercise List:

1. **Disassemble memory**
   Use the 'u' command to disassemble memory at the 'nt!NtCreateFile' function.

### Links:

* [u, u64, u2, u32 (disassemble virtual address)](https://docs.hyperdbg.org/commands/debugging-commands/u)