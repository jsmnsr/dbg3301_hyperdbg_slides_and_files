## Command Arguments

### Exercise List:

1. **Pass arguments to scripts**
   Create a '.ds' file script (e.g., use some print functions in the '?' command), and then pass parameters to this script file.

2. **Use arguments**
   Print the path of the '.ds' file using the '$arg0' pseudo-register.

### Links:

* [.script (run batch script commands)](https://docs.hyperdbg.org/commands/meta-commands/.script)

* [Debugger Script (DS) - Arguments](https://docs.hyperdbg.org/commands/scripting-language/debugger-script#arguments)