## Listing Modules

### Exercise List:

1. **List modules**
   Use the 'lm' command to list all user-mode and kernel-mode modules.
   
2. **List modules of special process**
   Use the 'lm' command to list all user-mode modules of a special process using its process id.
   
3. **Kernel-mode module starting with a special string**
   Use the 'lm' command to find all kernel-mode modules that start with 'nt'.

### Links:

* [lm (view loaded modules)](https://docs.hyperdbg.org/commands/debugging-commands/lm)