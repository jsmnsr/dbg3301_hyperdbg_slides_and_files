## Intro & Faults, Exceptions, Interrupts

### Exercise List:

1. **Find clock interrupts**
   Open the 'notepad.exe' and get its process id. You can use the '.start' command for this purpose. Create a script using the '!interrupt' command to monitor all clock-interrupts in the target process and print the context (general-purpose registers) using the 'printf' function.

### Links:

* [Part 8 - Files](https://url.hyperdbg.org/ost2/part-8-files)

* [!interrupt (hook external device interrupts)](https://docs.hyperdbg.org/commands/extension-commands/interrupt)

* [.start (start a new process)](https://docs.hyperdbg.org/commands/meta-commands/.start)

* [printf](https://docs.hyperdbg.org/commands/scripting-language/functions/exports/printf)