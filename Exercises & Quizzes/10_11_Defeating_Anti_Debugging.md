## Defeating Anti-Debugging Methods

### Exercise List:

1. **Compile the pafish**
   Compile the 'pafish' project (link below).

2. **Hide the debugger**
   Run the 'pafish' process while HyperDbg is not in the transparent-mode. Measure the default system characteristics using the '!measure' command. Enter the transparent-mode using the '!hide' command for the 'pafish' process. Now, compare the results of the 'pafish' in regular VMI Mode and the transparent-mode.

### Links:

* [pafish](https://github.com/a0rtega/pafish)

* [!measure (measuring and providing details for transparent-mode)](https://docs.hyperdbg.org/commands/extension-commands/measure)

* [!hide (enable transparent-mode)](https://docs.hyperdbg.org/commands/extension-commands/hide)

* [!unhide (disable transparent-mode)](https://docs.hyperdbg.org/commands/extension-commands/unhide)

* [Transparent Mode](https://docs.hyperdbg.org/tips-and-tricks/considerations/transparent-mode)