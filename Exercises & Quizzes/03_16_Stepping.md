## Stepping

### Exercise List:

1. **Step through the instructions**
   Use the 'p' to step-over and the 't' to step-in through the instructions.

2. **Step through instructions (Instrumentation step-in)**
   Use the 'i' command to step through the instructions by using the instrumentation step-in.

### Links:

* [i (instrumentation step-in)](https://docs.hyperdbg.org/commands/debugging-commands/i)

* [p (step-over)](https://docs.hyperdbg.org/commands/debugging-commands/p)

* [t (step-in)](https://docs.hyperdbg.org/commands/debugging-commands/t)