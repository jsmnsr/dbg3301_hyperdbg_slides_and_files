## Concepts

### Quizzes List:

1. What are the three types of actions that HyperDbg supports?

- The three types of actions that HyperDbg supports are: Break, Script, and Custom Codes.

2. What is the difference between "Break" action and "Script" action in HyperDbg?

- The "Break" action in HyperDbg is similar to classic debuggers. It pauses all cores and prevents them from executing instructions without the debugger's permission. This action is not available in local debugging. The "Script" action, on the other hand, allows you to view parameters, registers, and memory without breaking into the debugger. It's useful for analysis and logging. Unlike the "Break" action, the "Script" action doesn't pause the execution of cores.

3. How does the "Custom Codes" action work in HyperDbg?

- The "Custom Codes" action in HyperDbg enables you to execute your custom assembly code whenever a special event is triggered. This feature provides flexibility and allows you to tailor HyperDbg to your specific needs. It's a fast and powerful way to customize the behavior of HyperDbg based on the events that occur.

4. What do "regular" commands in HyperDbg apply to?

- "Regular" commands in HyperDbg apply to the debugging session and are used to control and obtain information from the debugging target. These commands are used for interacting with the target being debugged.

5. How are "meta" commands identified in HyperDbg, and what do they apply to?

- "Meta" commands in HyperDbg are identified by being prefixed with a dot, for example, ".test". These commands apply to the debugger itself, meaning they control the behavior of the debugger rather than interacting directly with the debugging target.

6. How are "extension" commands identified in HyperDbg, and what do they represent?

- "Extension" commands in HyperDbg are identified by being prefixed with an exclamation mark, for example, "!test". These commands are defined as debugger extensions and provide additional features or functionalities beyond the standard commands provided by HyperDbg. They can be used to access specific functionalities or tools integrated into the debugger.

### Links:

* [Prerequisites](https://docs.hyperdbg.org/using-hyperdbg/prerequisites)

* [Command style](https://docs.hyperdbg.org/contribution/style-guide/command-style)