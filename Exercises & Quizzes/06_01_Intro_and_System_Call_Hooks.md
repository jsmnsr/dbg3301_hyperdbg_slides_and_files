## Intro & System Call Hooks

### Exercise List:

1. **Syscall interception**
   Use the '!syscall' command to intercept any execution of the system-call number of 'nt!NtCreateFile'. You can find the system-call number from the j00ru's syscall table (link below).

2. **Print the name of the process**
   Use the 'printf' function along with the '$pname' Pseudo-register to print the name of the target process that executes this system-call.

3. **Print parameters**
   Print the parameters passed to the system-call (in x64 fastcall calling convention).

### Links:

* [j00ru's Windows X86-64 System Call Table (XP/2003/Vista/2008/7/2012/8/10)](https://j00ru.vexillium.org/syscalls/nt/64/)

* [hfiref0x's Combined Windows x64 syscall tables](https://github.com/hfiref0x/SyscallTables)

* [Part 6 - Files](https://url.hyperdbg.org/ost2/part-6-files)

* [!syscall, !syscall2 (hook system-calls)](https://docs.hyperdbg.org/commands/extension-commands/syscall)

* [Windows X86-64 System Call Table (XP/2003/Vista/2008/7/2012/8/10)](https://j00ru.vexillium.org/syscalls/nt/64/)

* [Pseudo-registers](https://docs.hyperdbg.org/commands/scripting-language/assumptions-and-evaluations#pseudo-registers)

* [printf](https://docs.hyperdbg.org/commands/scripting-language/functions/exports/printf)