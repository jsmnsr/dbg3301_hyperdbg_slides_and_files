## Tracing Heaven's Gate Instructions & System-call Servicing Routines

### Exercise List:

1. **Compile the system-call-test project**
   Compile 'system-call-test' project and get the process id, then intercept all SYSRET instructions (or returns from the system-calls).

2. **Trace the heaven's gate**
   Use the instrumentation step-in command to trace instruction from the user-mode to kernel-mode.

3. **Find FAR JMP and system-call number**
   Search through the resulted disassembly and find the system-call number (the 'RAX' or the 'EAX' register) as well as the FAR JMP or FAR CALL instruction used for entering the heaven's gate.

### Links:

* [system-call-test](https://url.hyperdbg.org/ost2/system-call-test)

* [j00ru's Windows X86-64 System Call Table (XP/2003/Vista/2008/7/2012/8/10)](https://j00ru.vexillium.org/syscalls/nt/64)

* [i (instrumentation step-in)](https://docs.hyperdbg.org/commands/debugging-commands/i)