## MSRs

### Exercise List:

1. **Read the Model-Specific Registers (MSRs)**
   Use the 'rdmsr' command to read the MSR\_LSTAR (0x80000082).

2. **Detect any reads to MSRs**
   Use the '!msrread' command to view all the MSRs that Windows (and system drivers) try to read and print the MSR Code (the 'RCX' register).
   
3. **Detect any writes to MSRs**
   Use the '!msrwrite' command to view all the MSRs that Windows (and system drivers) try to write and print the MSR Code (the 'RCX' register).

### Links:

* [Part 8 - Files](https://url.hyperdbg.org/ost2/part-8-files)

* [MSR-list](https://url.hyperdbg.org/ost2/msr-list)

* [rdmsr (read model-specific register)](https://docs.hyperdbg.org/commands/debugging-commands/rdmsr)

* [!msrread (hook RDMSR instruction execution)](https://docs.hyperdbg.org/commands/extension-commands/msrread)

* [!msrwrite (hook WRMSR instruction execution)](https://docs.hyperdbg.org/commands/extension-commands/msrwrite)