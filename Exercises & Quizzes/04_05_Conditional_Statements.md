## Conditional Statements

### Exercise List:

1. **Use nested ifs**
   Use nested-ifs (if, elsif, else) statements and check for different conditions in the system (e.g., check the 'RAX' register equals to '0x55' and if not change it to '0x85').

### Links:

* [? (evaluate and execute expressions and scripts in debuggee)](https://docs.hyperdbg.org/commands/debugging-commands/eval)

* [Conditionals & Loops](https://docs.hyperdbg.org/commands/scripting-language/conditionals-and-loops)