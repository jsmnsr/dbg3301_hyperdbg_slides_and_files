## Memory Considerations

### Quizzes List:

1. Can you explain the difference between "safe" and "unsafe" behavior in HyperDbg?

- The difference between "safe" and "unsafe" behavior in HyperDbg is related to the impact on system stability. "Safe" behavior refers to actions that work reliably without causing a system crash or halt. On the other hand, "unsafe" behavior involves actions that have the potential to destabilize the system, potentially leading to crashes or halts.

2. Why should you avoid "unsafe" behavior in HyperDbg?

- "Unsafe" behavior should be avoided in HyperDbg because it can lead to system instability, crashes, or halts. Given the complexity of managing code in vmx root-mode, actions that are considered "unsafe" could cause interrupts to be masked, data transfers to fail, or memory access to become problematic. Avoiding such behavior helps maintain the overall stability of the system.

3. In which mode of execution are interrupts masked (disabled) in HyperDbg?

- Interrupts are masked (disabled) in the vmx-root mode of execution in HyperDbg. This means that hardware interrupts that could trigger context switches or handle various events are temporarily disabled while in this mode.

4. What is the purpose of HyperDbg's safe access to the non-paged pool in user, kernel, and vmx-root modes?

- HyperDbg's purpose in providing safe access to the non-paged pool in user, kernel, and vmx-root modes is to enable developers to interact with system resources in a controlled and reliable manner. By offering a safe way to access the non-paged pool, HyperDbg ensures that actions involving memory access and data transfers are executed in a way that minimizes the risk of system instability and crashes.

5. What are the potential consequences of "unsafe" behavior in HyperDbg?

- The potential consequences of "unsafe" behavior in HyperDbg can include system instability, crashes, or halts. For example, using certain NT APIs with IRQL limitations or accessing a paged-pool while in vmx root mode could lead to system halts or crashes. Directly accessing user-mode codes from vmx-root mode might destabilize the system due to differences in memory structures and caching. Additionally, failing to manage cr3 (control register 3) properly when accessing process memory can lead to system crashes. In essence, "unsafe" behavior in HyperDbg poses a risk to the overall stability and reliability of the system.

### Links:

* [The "unsafe" behavior](https://docs.hyperdbg.org/tips-and-tricks/considerations/the-unsafe-behavior)