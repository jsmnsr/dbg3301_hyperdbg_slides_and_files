## RDTSC/RDTSCP Instruction Hooking

### Exercise List:

1. **Print RDTSC/RDTSCP instruction in the user-mode**
   Print the 'RIP' register along with the process name and the process id of any instances of RDTSC/RDTSCP instructions that are running into the user-mode area. Note that you can detect whether the debuggee is in the kernel-mode or the user-mode by checking the most significant bit of the 'RIP' register. If set, it shows that the target is operating in the kernel-mode; otherwise, it's operating in the user-mode.

2. **Print RDTSC/RDTSCP instruction in pafish**
   Compile the 'pafish' program, and write a script to print the 'RIP' register of any 'RDTSC', or 'RDTSCP' instruction that is used in this program.

### Links:

* [Pafish](https://github.com/a0rtega/pafish)

* [!tsc (hook RDTSC/RDTSCP instruction execution)](https://docs.hyperdbg.org/commands/extension-commands/tsc)

* [printf](https://docs.hyperdbg.org/commands/scripting-language/functions/exports/printf)

* [Pseudo-registers](https://docs.hyperdbg.org/commands/scripting-language/assumptions-and-evaluations#pseudo-registers)