## Debugging Processes & Threads

### Exercise List:

1. **Show current process**
   Show the current process using the '.process' command.

2. **Show current thread**
   Show the current thread using the '.thread' command.

3. **Show list of processes**
   Show a list of currently running processes (Configure symbols before using it).

4. **Show a list of threads for a process**
   Show a list of threads of the target process.

5. **Switch to a new process**
   Switch to a different process using the '.process' and the '.process2' commands.

### Links:

* [.process, .process2 (show the current process and switch to another process)](https://docs.hyperdbg.org/commands/meta-commands/.process)

* [.thread, .thread2 (show the current thread and switch to another thread)](https://docs.hyperdbg.org/commands/meta-commands/.thread)

* [Switching to a Specific Process or Thread](https://docs.hyperdbg.org/using-hyperdbg/user-mode-debugging/examples/basics/switching-to-a-specific-process-or-thread)

* [Difference between process and thread switching commands](https://docs.hyperdbg.org/tips-and-tricks/considerations/difference-between-process-and-thread-switching-commands)