## Customizing Settings

### Exercise List:

1. **Customize settings**
   Use the 'settings' command to change the syntax of the assembler (e.g., choose 'at&t' syntax), then use the 'u' command to disassemble a random address and see the results.

### Links:

* [settings (configures different options and preferences)](https://docs.hyperdbg.org/commands/debugging-commands/settings)

* [u, u64, u2, u32 (disassemble virtual address)](https://docs.hyperdbg.org/commands/debugging-commands/u)