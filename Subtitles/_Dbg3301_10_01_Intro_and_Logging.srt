1
00:00:00,000 --> 00:00:06,180
hi everyone welcome to the next part of

2
00:00:03,179 --> 00:00:09,059
the tutorial reversing with HyperDbg

3
00:00:06,180 --> 00:00:12,860
this tutorial is about tricks that we

4
00:00:09,059 --> 00:00:17,100
can use in hyperview G debugger so let's

5
00:00:12,860 --> 00:00:21,539
see a brief outline of what we want to

6
00:00:17,100 --> 00:00:24,240
talk in this section first we see how

7
00:00:21,539 --> 00:00:26,460
lag open and lock close works in hyper

8
00:00:24,240 --> 00:00:29,340
dvg then we see some of the batch

9
00:00:26,460 --> 00:00:33,140
scripts that you can use in HyperDbg

10
00:00:29,340 --> 00:00:36,000
and you see how we can examine different

11
00:00:33,140 --> 00:00:40,200
processor features then we have P

12
00:00:36,000 --> 00:00:42,899
parsing and see how we can customize the

13
00:00:40,200 --> 00:00:46,860
builds of the HyperDbg and customize

14
00:00:42,899 --> 00:00:51,360
the things then we will see how hyper

15
00:00:46,860 --> 00:00:54,719
dvg can be used in DFIR community we'll

16
00:00:51,360 --> 00:00:57,660
see how ignoring events work and at last

17
00:00:54,719 --> 00:01:00,600
we will explore how to defeat some of

18
00:00:57,660 --> 00:01:02,460
that even anti-debugging methods so

19
00:01:00,600 --> 00:01:05,519
let's see the first thing that we're

20
00:01:02,460 --> 00:01:09,240
gonna talk about is logo pen unlock

21
00:01:05,519 --> 00:01:12,780
clothes there are two commands same

22
00:01:09,240 --> 00:01:16,920
as exactly same as WinDbg for creating

23
00:01:12,780 --> 00:01:19,799
logs from the debugger outputs that you

24
00:01:16,920 --> 00:01:24,360
can use these two commands the first

25
00:01:19,799 --> 00:01:29,520
command is Lago pen dot slug open or

26
00:01:24,360 --> 00:01:34,200
this command gets file a text file as is

27
00:01:29,520 --> 00:01:37,020
old as its input and I will save the

28
00:01:34,200 --> 00:01:41,520
debugger whatever you type or whatever

29
00:01:37,020 --> 00:01:44,820
you see in the debugger console then it

30
00:01:41,520 --> 00:01:47,520
will be saved to the target file and

31
00:01:44,820 --> 00:01:50,159
after we finish we no longer need to

32
00:01:47,520 --> 00:01:53,640
save the debugger outputs we could use

33
00:01:50,159 --> 00:01:58,439
that lag closed command to close the

34
00:01:53,640 --> 00:02:00,899
file and these commands is really useful

35
00:01:58,439 --> 00:02:04,079
if you want to get a history story of

36
00:02:00,899 --> 00:02:06,680
what what are what commands executed in

37
00:02:04,079 --> 00:02:06,680
the debugging system

