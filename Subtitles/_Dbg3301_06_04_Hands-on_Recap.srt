1
00:00:00,000 --> 00:00:09,420
and now we're gonna see a hands-on 

2
00:00:05,759 --> 00:00:11,700
and some examples of changing

3
00:00:09,420 --> 00:00:13,139
parameters of a system call by using

4
00:00:11,700 --> 00:00:15,780
HyperDbg

5
00:00:13,139 --> 00:00:19,080
also we're gonna change the results of

6
00:00:15,780 --> 00:00:21,900
the system call so let's see the

7
00:00:19,080 --> 00:00:27,960
example so I put the source code for the

8
00:00:21,900 --> 00:00:31,700
 examples here in the scripts part

9
00:00:27,960 --> 00:00:37,020
 the first example is that to try to

10
00:00:31,700 --> 00:00:40,920
 debug a special system call so

11
00:00:37,020 --> 00:00:45,180
let's say that we want to debug zero

12
00:00:40,920 --> 00:00:47,820
it's 55 which which is the system

13
00:00:45,180 --> 00:00:53,100
call number for NtCreateFile

14
00:00:47,820 --> 00:00:55,680
and  I want to debug this

15
00:00:53,100 --> 00:00:57,680
special process so let's see it in

16
00:00:55,680 --> 00:01:02,360
action

17
00:00:57,680 --> 00:01:02,360
 I will try to continue

18
00:01:08,960 --> 00:01:16,260
 run the run this 

19
00:01:12,840 --> 00:01:18,240
again I have the process ID and updated

20
00:01:16,260 --> 00:01:20,600
version of the price Society I put the

21
00:01:18,240 --> 00:01:24,840
plus side here

22
00:01:20,600 --> 00:01:28,080
and it just prints the system calls and

23
00:01:24,840 --> 00:01:31,259
if the system call number equals to zero

24
00:01:28,080 --> 00:01:35,759
is five five then pauses the entire

25
00:01:31,259 --> 00:01:38,960
session so I again I pause

26
00:01:35,759 --> 00:01:38,960
this man

27
00:01:39,299 --> 00:01:46,979
I produce you run this system call and

28
00:01:43,939 --> 00:01:49,320
continue the normal execution so I press

29
00:01:46,979 --> 00:01:51,659
enter here whenever I press enter here

30
00:01:49,320 --> 00:01:56,040
as we see here

31
00:01:51,659 --> 00:01:56,040
price to run 

32
00:01:59,060 --> 00:02:04,520
as you see

33
00:02:01,439 --> 00:02:04,520
here actually

34
00:02:05,520 --> 00:02:10,739
they're a break point it tries to run

35
00:02:08,700 --> 00:02:13,140
NtCreateFile

36
00:02:10,739 --> 00:02:14,940
so first we have we have to see a

37
00:02:13,140 --> 00:02:18,239
breakpoint

38
00:02:14,940 --> 00:02:20,940
nothing is executed yet we don't see any

39
00:02:18,239 --> 00:02:24,300
syscall number here it's just a simple

40
00:02:20,940 --> 00:02:27,540
breakpoint and when I continue the debug

41
00:02:24,300 --> 00:02:32,400
normally then we can see that the system

42
00:02:27,540 --> 00:02:35,640
call number 55 is executed and now we

43
00:02:32,400 --> 00:02:38,160
are in the middle of executing a system

44
00:02:35,640 --> 00:02:39,599
called pause the system system on a

45
00:02:38,160 --> 00:02:43,080
system call

46
00:02:39,599 --> 00:02:45,120
so I can see the parameters here

47
00:02:43,080 --> 00:02:47,760
this these are this is the first

48
00:02:45,120 --> 00:02:48,920
parameter to this system call this is

49
00:02:47,760 --> 00:02:52,560
the

50
00:02:48,920 --> 00:02:54,180
second parameter and this is the third

51
00:02:52,560 --> 00:02:58,519
and

52
00:02:54,180 --> 00:02:58,519
fourth parameter this system

53
00:02:58,980 --> 00:03:06,260
again I have a example here

54
00:03:02,959 --> 00:03:10,980
in NtCreateFile

55
00:03:06,260 --> 00:03:13,760
the execution is let's just show the

56
00:03:10,980 --> 00:03:13,760
integrate file

57
00:03:15,060 --> 00:03:17,540
yeah

58
00:03:17,700 --> 00:03:24,420
 in Antichrist file 

59
00:03:21,599 --> 00:03:29,400
which is the kernel function both an

60
00:03:24,420 --> 00:03:29,400
ntdl and kernel function 

61
00:03:31,280 --> 00:03:36,540
here's the way that we pass the

62
00:03:34,760 --> 00:03:39,239


63
00:03:36,540 --> 00:03:42,120
parameters as you can see in the third

64
00:03:39,239 --> 00:03:44,220
parameters we have object attributes and

65
00:03:42,120 --> 00:03:46,500
here in this example

66
00:03:44,220 --> 00:03:49,700


67
00:03:46,500 --> 00:03:53,760
I just want to enter step the third 

68
00:03:49,700 --> 00:03:55,819
parameter and it's a simple printf

69
00:03:53,760 --> 00:03:58,620
which prints

70
00:03:55,819 --> 00:04:02,280
 ads 

71
00:03:58,620 --> 00:04:05,879
10 bytes to the r8 or third parameters

72
00:04:02,280 --> 00:04:08,459
and adds a series 8 to them and then

73
00:04:05,879 --> 00:04:12,659
finally gear for instance everything and

74
00:04:08,459 --> 00:04:16,500
shape and show it on 

75
00:04:12,659 --> 00:04:19,139
format of a Unicode string and this is

76
00:04:16,500 --> 00:04:21,740
the same as the previous example that

77
00:04:19,139 --> 00:04:23,840
we've seen in the previous sessions

78
00:04:21,740 --> 00:04:27,120
in the previous session

79
00:04:23,840 --> 00:04:31,919
we 

80
00:04:27,120 --> 00:04:35,520
so we did the same thing for object

81
00:04:31,919 --> 00:04:36,800
attributes we saw it previously so I

82
00:04:35,520 --> 00:04:40,139
just

83
00:04:36,800 --> 00:04:42,900
avoid to re-explaining it and just write

84
00:04:40,139 --> 00:04:45,560
execute it

85
00:04:42,900 --> 00:04:50,699
as you can see that

86
00:04:45,560 --> 00:04:53,660
this function this command prints the

87
00:04:50,699 --> 00:04:58,520
print the the name that

88
00:04:53,660 --> 00:05:04,259
name of the file that we try to

89
00:04:58,520 --> 00:05:07,500
change or try to open in our system so I

90
00:05:04,259 --> 00:05:11,160
just try I I

91
00:05:07,500 --> 00:05:15,240
if I want to print the exact address of

92
00:05:11,160 --> 00:05:17,580
this parameter in the user mode I use

93
00:05:15,240 --> 00:05:19,740
this command it's exactly the same as

94
00:05:17,580 --> 00:05:23,100
the above command but instead of showing

95
00:05:19,740 --> 00:05:26,039
in showing it in Unicode format it just

96
00:05:23,100 --> 00:05:28,979
simply shows the address so if I want to

97
00:05:26,039 --> 00:05:32,039
see the address by using DC command you

98
00:05:28,979 --> 00:05:35,100
can see that it's available on

99
00:05:32,039 --> 00:05:37,759
 question

100
00:05:35,100 --> 00:05:40,620
this address so

101
00:05:37,759 --> 00:05:44,759
 here I just want to have a simple

102
00:05:40,620 --> 00:05:47,880
modification here I want to change the

103
00:05:44,759 --> 00:05:50,039
name here let's say

104
00:05:47,880 --> 00:05:53,820


105
00:05:50,039 --> 00:05:59,419
this is the HyperDbg and I want to

106
00:05:53,820 --> 00:05:59,419
intercept this change this address to

107
00:06:04,860 --> 00:06:11,280
this is for p this is for 

108
00:06:09,560 --> 00:06:14,580
backslash

109
00:06:11,280 --> 00:06:16,620
and here this this should be something

110
00:06:14,580 --> 00:06:18,720
for H

111
00:06:16,620 --> 00:06:23,580
for this one

112
00:06:18,720 --> 00:06:27,419
and this should be for y so I just try

113
00:06:23,580 --> 00:06:29,699
to set it to a known

114
00:06:27,419 --> 00:06:33,660


115
00:06:29,699 --> 00:06:38,520
letter for example a a capital A which

116
00:06:33,660 --> 00:06:40,819
it's hex numbers or 41

117
00:06:38,520 --> 00:06:45,240
and again

118
00:06:40,819 --> 00:06:50,479
change the Y here to

119
00:06:45,240 --> 00:06:50,479
b or 42 for two

120
00:06:51,600 --> 00:06:53,940


121
00:06:52,280 --> 00:06:57,900
P2

122
00:06:53,940 --> 00:07:00,380
C yeah here's just just a simple example

123
00:06:57,900 --> 00:07:05,160
of I just want to show you how we can

124
00:07:00,380 --> 00:07:08,819
modify these parameters and also we

125
00:07:05,160 --> 00:07:11,340
ranked before I mean we have this

126
00:07:08,819 --> 00:07:14,039
file already because this is the second

127
00:07:11,340 --> 00:07:17,280
time that we try to run this system

128
00:07:14,039 --> 00:07:20,400
called process so we already have the

129
00:07:17,280 --> 00:07:22,440
file but the file is the file that is

130
00:07:20,400 --> 00:07:24,900
here is not for

131
00:07:22,440 --> 00:07:28,020
Uh current run

132
00:07:24,900 --> 00:07:33,060
the current run of the program is not

133
00:07:28,020 --> 00:07:36,120
yet executed so the file is not made yet

134
00:07:33,060 --> 00:07:41,240
I just modified the file name to

135
00:07:36,120 --> 00:07:46,380
something like a b C d e r d b g

136
00:07:41,240 --> 00:07:49,020
and before continuing the system the

137
00:07:46,380 --> 00:07:52,039
system call parameters are no modified

138
00:07:49,020 --> 00:07:55,319
so I continue the execution

139
00:07:52,039 --> 00:07:59,340
yeah what you can see here is the list

140
00:07:55,319 --> 00:08:02,580
of the system calls that in this

141
00:07:59,340 --> 00:08:05,099
example I try to print the system call

142
00:08:02,580 --> 00:08:07,800
number for all the system console here

143
00:08:05,099 --> 00:08:10,440
we have a list of system calls here

144
00:08:07,800 --> 00:08:13,800
this system calls relate to the Graphic

145
00:08:10,440 --> 00:08:16,880
or win 32k and this system calls relate

146
00:08:13,800 --> 00:08:16,880
to ntos kernel

147
00:08:17,479 --> 00:08:23,099
and as you can see that the file name is

148
00:08:20,460 --> 00:08:27,780
modified it's the name is not hyper

149
00:08:23,099 --> 00:08:31,259
division anymore it's ABC ER dbg and

150
00:08:27,780 --> 00:08:36,060
again we have the same parameters here

151
00:08:31,259 --> 00:08:40,380
now let's see the last example in this

152
00:08:36,060 --> 00:08:44,779
example we again use the same file

153
00:08:40,380 --> 00:08:46,380
I just want to delete this files

154
00:08:44,779 --> 00:08:47,160
and

155
00:08:46,380 --> 00:08:49,760


156
00:08:47,160 --> 00:08:49,760
yeah

157
00:08:56,279 --> 00:09:00,240
okay

158
00:08:57,480 --> 00:09:02,220
I actually if you want to see the

159
00:09:00,240 --> 00:09:04,920
events

160
00:09:02,220 --> 00:09:06,360
you have these events actually one word

161
00:09:04,920 --> 00:09:11,339
system

162
00:09:06,360 --> 00:09:15,019
not important I'm sorry hear them all

163
00:09:11,339 --> 00:09:15,019
I'm connecting with the dividing

164
00:09:20,940 --> 00:09:27,540
okay I just run this exam this process

165
00:09:24,600 --> 00:09:31,320
again and here is the process ID along

166
00:09:27,540 --> 00:09:33,680
with trade ID I change the thread ID

167
00:09:31,320 --> 00:09:33,680
here

168
00:09:36,440 --> 00:09:45,480
in this example like what we see in

169
00:09:40,740 --> 00:09:48,480
the previous slides and then we

170
00:09:45,480 --> 00:09:55,500
in the system cooking we see this

171
00:09:48,480 --> 00:09:59,580
example that we try to modify the

172
00:09:55,500 --> 00:10:02,640
 that we want to modify or we want to

173
00:09:59,580 --> 00:10:06,300
see the results of this C Scott CIS

174
00:10:02,640 --> 00:10:09,680
rate instructions and in this example I

175
00:10:06,300 --> 00:10:12,959
try to modify the RX which is the return

176
00:10:09,680 --> 00:10:18,000
 value from the kernel to the user

177
00:10:12,959 --> 00:10:22,440
mode to this special value

178
00:10:18,000 --> 00:10:25,160
so so let's let's see the action 

179
00:10:22,440 --> 00:10:25,160
okay

180
00:10:30,920 --> 00:10:36,380
Iran is the these s scripts here

181
00:10:39,680 --> 00:10:48,899
yeah it was okay simple continue that

182
00:10:44,940 --> 00:10:51,480
you already normally and or we forget

183
00:10:48,899 --> 00:10:56,100
to change the

184
00:10:51,480 --> 00:11:00,480
name of the thread ID

185
00:10:56,100 --> 00:11:03,440
so here is the thread ID

186
00:11:00,480 --> 00:11:06,980
I will

187
00:11:03,440 --> 00:11:06,980
manipulate it here

188
00:11:13,140 --> 00:11:16,800
run these commands again on high produce

189
00:11:15,720 --> 00:11:19,260
G

190
00:11:16,800 --> 00:11:23,640
thank you the execution and press enter

191
00:11:19,260 --> 00:11:25,079
now we should see the systems halted

192
00:11:23,640 --> 00:11:28,339
because of

193
00:11:25,079 --> 00:11:28,339
this breakpoint

194
00:11:28,920 --> 00:11:35,660
and I continue the gain but and as you

195
00:11:32,339 --> 00:11:39,660
can see here that the application is 

196
00:11:35,660 --> 00:11:43,019
executed normally and you receive

197
00:11:39,660 --> 00:11:46,399
failed to create file error and this is

198
00:11:43,019 --> 00:11:48,959
the error this is the exact error that

199
00:11:46,399 --> 00:11:52,980
 we put here

200
00:11:48,959 --> 00:11:54,660
so we changed it and you can see you can

201
00:11:52,980 --> 00:11:59,279
see the file

202
00:11:54,660 --> 00:11:59,279
is also empty 

203
00:11:59,519 --> 00:12:04,279
the files empty because 

204
00:12:05,220 --> 00:12:15,060
here if it was not successful then we

205
00:12:10,980 --> 00:12:20,060
return with an error so the content this

206
00:12:15,060 --> 00:12:20,060
hello world won't be written on the file

