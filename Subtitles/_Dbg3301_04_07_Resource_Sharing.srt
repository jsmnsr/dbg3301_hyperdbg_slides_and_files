1
00:00:00,160 --> 00:00:09,559
now let's see another important part

2
00:00:04,480 --> 00:00:12,759
of the hyper dvgs script engine which

3
00:00:09,559 --> 00:00:15,400
is resource CH as you can see in the

4
00:00:12,759 --> 00:00:18,240
previous slides we have local

5
00:00:15,400 --> 00:00:20,720
variables global variables global

6
00:00:18,240 --> 00:00:24,199
variables are those variable that start

7
00:00:20,720 --> 00:00:27,519
with the dot prefix thing is how we can

8
00:00:24,199 --> 00:00:30,960
share them between different ches as you

9
00:00:27,519 --> 00:00:34,800
can see this is a classic operat system

10
00:00:30,960 --> 00:00:38,640
like problems like you cannot access a

11
00:00:34,800 --> 00:00:41,280
variable simultaneously from two cores

12
00:00:38,640 --> 00:00:44,800
because in this way it's not

13
00:00:41,280 --> 00:00:46,199
guaranteed to give you a correct answer

14
00:00:44,800 --> 00:00:49,120
if you want to access them

15
00:00:46,199 --> 00:00:53,039
simultaneously in different courses so

16
00:00:49,120 --> 00:00:56,239
we can use if we want to use global Val

17
00:00:53,039 --> 00:00:58,600
or if we want to use a shared object in

18
00:00:56,239 --> 00:01:01,719
the script engine of the hyper DVD we

19
00:00:58,600 --> 00:01:05,799
can use the SP lock functions and in

20
00:01:01,719 --> 00:01:10,720
this es functions we should use global

21
00:01:05,799 --> 00:01:14,159
variables so if you want to use 

22
00:01:10,720 --> 00:01:17,720
events or do something simultaneously

23
00:01:14,159 --> 00:01:20,360
events might use it keep in mind that 

24
00:01:17,720 --> 00:01:23,119
locks are all of the time global

25
00:01:20,360 --> 00:01:25,680
variables for example I want to access

26
00:01:23,119 --> 00:01:29,159
global variable which is called my

27
00:01:25,680 --> 00:01:31,799
global VAR global where and I will

28
00:01:29,159 --> 00:01:34,640
access it between an es loock lock which

29
00:01:31,799 --> 00:01:39,040
is a function I will explain later but

30
00:01:34,640 --> 00:01:41,840
for now we will pass the M do my global

31
00:01:39,040 --> 00:01:45,119
variable lock here lock so it's

32
00:01:41,840 --> 00:01:48,360
guaranteed that no other cores will

33
00:01:45,119 --> 00:01:51,399
execute this statement and after that I

34
00:01:48,360 --> 00:01:53,399
will release the luck so other cores

35
00:01:51,399 --> 00:01:55,920
might get a chance to execute this

36
00:01:53,399 --> 00:01:55,920
lineup

