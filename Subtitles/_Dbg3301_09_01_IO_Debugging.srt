1
00:00:00,000 --> 00:00:06,660
hi everyone welcome to the next part of

2
00:00:03,179 --> 00:00:09,360
the tutorial reversing with HyperDbg in

3
00:00:06,660 --> 00:00:12,420
this part we're gonna talk about IO

4
00:00:09,360 --> 00:00:15,260
debugging here is a brief outline of

5
00:00:12,420 --> 00:00:18,779
this session we're gonna see about

6
00:00:15,260 --> 00:00:22,279
external interrupts you'll see

7
00:00:18,779 --> 00:00:25,260
communicating with different peripherals

8
00:00:22,279 --> 00:00:28,199
portmap.io or PMIO devices memory

9
00:00:25,260 --> 00:00:31,859
mapped IO and then we compare PMIO and

10
00:00:28,199 --> 00:00:35,040
MMIO then we have a hands-on and in

11
00:00:31,859 --> 00:00:38,340
which we monitor and modify IO cores

12
00:00:35,040 --> 00:00:40,620
for PS2 keywords and now let's talk a

13
00:00:38,340 --> 00:00:42,960
little bit about external interrupts

14
00:00:40,620 --> 00:00:46,100
this is the same type of that we also

15
00:00:42,960 --> 00:00:50,520
covered in the previous session

16
00:00:46,100 --> 00:00:54,059
in previous sessions how interrupts are

17
00:00:50,520 --> 00:00:56,460
handled in processor and how a processor

18
00:00:54,059 --> 00:00:59,100
and how HyperDbg can intercept

19
00:00:56,460 --> 00:01:02,820
interrupts and then and then perform

20
00:00:59,100 --> 00:01:06,360
modification and we saw that it's

21
00:01:02,820 --> 00:01:09,000
possible to do these tests by using bang

22
00:01:06,360 --> 00:01:11,220
interrupt command for example in most of

23
00:01:09,000 --> 00:01:14,400
the cases when external device is any

24
00:01:11,220 --> 00:01:16,560
external devices that wants to notify

25
00:01:14,400 --> 00:01:19,320
the processor that something is

26
00:01:16,560 --> 00:01:22,860
happening in that special device then it

27
00:01:19,320 --> 00:01:25,200
creates a interrupt and this way it

28
00:01:22,860 --> 00:01:27,960
notifies the processor for example when

29
00:01:25,200 --> 00:01:30,840
we press a key on our keyboard then

30
00:01:27,960 --> 00:01:33,780
traptism worked and it's responsibility

31
00:01:30,840 --> 00:01:36,000
of the processor to handle the keyboard

32
00:01:33,780 --> 00:01:38,400
and and it just notifies the operating

33
00:01:36,000 --> 00:01:41,340
system and everything is handled inside

34
00:01:38,400 --> 00:01:43,920
the operating system so most of the

35
00:01:41,340 --> 00:01:46,799
times when the Interrupters receive it's

36
00:01:43,920 --> 00:01:50,700
the responsibility of APIC device to

37
00:01:46,799 --> 00:01:54,299
handle and deliver it to the processor

38
00:01:50,700 --> 00:01:57,720
and and we have two types of APIC

39
00:01:54,299 --> 00:02:00,720
devices x2a pick and X AP devices which

40
00:01:57,720 --> 00:02:03,960
is an older version first one handles

41
00:02:00,720 --> 00:02:07,259
through MSR registers and the second one

42
00:02:03,960 --> 00:02:10,679
or xap is handled by base addressed by

43
00:02:07,259 --> 00:02:13,620
physical address as HyperDbg is a

44
00:02:10,679 --> 00:02:16,620
hypervisor level divider then it's a

45
00:02:13,620 --> 00:02:20,220
notified about the interrupts before the

46
00:02:16,620 --> 00:02:22,200
operating system so it's in this stage

47
00:02:20,220 --> 00:02:24,180
We could decide whether we want to pass

48
00:02:22,200 --> 00:02:26,660
the interrupt to the operating system or

49
00:02:24,180 --> 00:02:29,340
we can handle it or ignore it without

50
00:02:26,660 --> 00:02:31,800
re-injecting the event or the interrupt

51
00:02:29,340 --> 00:02:34,560
to the operating system now let's see

52
00:02:31,800 --> 00:02:37,560
some of the basic concepts relating to

53
00:02:34,560 --> 00:02:40,200
the communication of the peripherals in

54
00:02:37,560 --> 00:02:42,840
HyperDbg for intercepting the

55
00:02:40,200 --> 00:02:45,599
communication there are two options and

56
00:02:42,840 --> 00:02:48,900
the first option is to manage external

57
00:02:45,599 --> 00:02:50,940
devices by intercepting interrupts for

58
00:02:48,900 --> 00:02:53,819
example we got notified whenever a

59
00:02:50,940 --> 00:02:56,879
device wants to connect to the operating

60
00:02:53,819 --> 00:02:58,980
system to send an input or an output

61
00:02:56,879 --> 00:03:02,519
this is done by using the interrupt

62
00:02:58,980 --> 00:03:05,879
command and are also other possible ways

63
00:03:02,519 --> 00:03:08,580
of handling or debugging IO devices

64
00:03:05,879 --> 00:03:10,860
basically I once generally speak about

65
00:03:08,580 --> 00:03:13,379
it that we have two types of

66
00:03:10,860 --> 00:03:16,319
communication between the devices and

67
00:03:13,379 --> 00:03:20,940
the processor is either port mapped IO

68
00:03:16,319 --> 00:03:24,080
or PMIO or memory mapped IO or MMIO I

69
00:03:20,940 --> 00:03:26,459
think these methods are responsible for

70
00:03:24,080 --> 00:03:28,860
performing input and output and

71
00:03:26,459 --> 00:03:31,800
communicating between different profiles

72
00:03:28,860 --> 00:03:34,739
processors and the operating system so

73
00:03:31,800 --> 00:03:38,400
if the device is in most of the devices

74
00:03:34,739 --> 00:03:41,879
that are connected through the PCIe or

75
00:03:38,400 --> 00:03:45,060
PCI these devices generally use memory

76
00:03:41,879 --> 00:03:47,879
mapped IO and the initial configuration

77
00:03:45,060 --> 00:03:53,220
of these devices are done by using PMI

78
00:03:47,879 --> 00:03:56,340
or MMIO is the preferred method in x86

79
00:03:53,220 --> 00:04:00,480
based processors the thing is that AMD

80
00:03:56,340 --> 00:04:03,420
did not for the 64-bit processor AMD did

81
00:04:00,480 --> 00:04:06,720
not extend the IO port instructions

82
00:04:03,420 --> 00:04:10,560
when defining the new x86 architecture

83
00:04:06,720 --> 00:04:13,379
and now let's see a port mapped IO or

84
00:04:10,560 --> 00:04:16,320
PMIO ndk where's my fio which is also

85
00:04:13,379 --> 00:04:19,560
called isolated IO use two instructions

86
00:04:16,320 --> 00:04:22,620
for performing IO operations these

87
00:04:19,560 --> 00:04:26,100
instructions are in and out and these

88
00:04:22,620 --> 00:04:28,740
instructions have several variants so

89
00:04:26,100 --> 00:04:31,620
some of them copy one by two bytes or

90
00:04:28,740 --> 00:04:33,960
four bytes it's done between ex and a

91
00:04:31,620 --> 00:04:37,620
special port HyperDbg the other

92
00:04:33,960 --> 00:04:40,860
hand exports this functionality or make

93
00:04:37,620 --> 00:04:44,759
the debugging of the IO ports possible

94
00:04:40,860 --> 00:04:48,360
by using two commands IO in or bang ion

95
00:04:44,759 --> 00:04:51,600
and bang or exclamation mark IO out the

96
00:04:48,360 --> 00:04:53,880
first one is used for intercepting in

97
00:04:51,600 --> 00:04:57,300
instructions and the second one is used

98
00:04:53,880 --> 00:05:00,240
for intercepting out instructions and we

99
00:04:57,300 --> 00:05:03,180
could use the script engine here to to

100
00:05:00,240 --> 00:05:05,880
notify about the about this

101
00:05:03,180 --> 00:05:07,979
communication between peripherals even

102
00:05:05,880 --> 00:05:11,100
before the operating system and perform

103
00:05:07,979 --> 00:05:14,820
some changes and the operating system

104
00:05:11,100 --> 00:05:18,419
will never know that the input or output

105
00:05:14,820 --> 00:05:21,840
is modified and here is a simple script

106
00:05:18,419 --> 00:05:24,419
that shows a where for example this port

107
00:05:21,840 --> 00:05:27,479
IO port is read by using a simple

108
00:05:24,419 --> 00:05:31,259
printf in the script engine other than

109
00:05:27,479 --> 00:05:35,580
that PMIO we have we also have MMIO here

110
00:05:31,259 --> 00:05:39,720
or memory mapped IO devices memory is

111
00:05:35,580 --> 00:05:42,180
not just RAM addresses and the memory

112
00:05:39,720 --> 00:05:44,460
itself is occupied by RAM and a small

113
00:05:42,180 --> 00:05:46,979
fraction of total address spaces are

114
00:05:44,460 --> 00:05:50,940
relating to the processor the reminder

115
00:05:46,979 --> 00:05:54,000
of the memory itself is used for MMIO

116
00:05:50,940 --> 00:05:57,120
devices including the PCI and PCIe

117
00:05:54,000 --> 00:06:00,120
devices and the processor routes these

118
00:05:57,120 --> 00:06:02,340
accesses to these addresses by

119
00:06:00,120 --> 00:06:04,080
configuring appropriate device registers

120
00:06:02,340 --> 00:06:06,240
whenever the processor wants to

121
00:06:04,080 --> 00:06:10,440
enumerate the BIOS or the operating

122
00:06:06,240 --> 00:06:12,840
system wants to configure this space at

123
00:06:10,440 --> 00:06:16,380
the initialization at this point there

124
00:06:12,840 --> 00:06:18,539
is a special way to access the devices

125
00:06:16,380 --> 00:06:22,620
and it's called PCI configuration

126
00:06:18,539 --> 00:06:25,440
address space in this way to IO two

127
00:06:22,620 --> 00:06:28,139
PMIO ports as you can see its address

128
00:06:25,440 --> 00:06:30,840
here are used to access the

129
00:06:28,139 --> 00:06:33,360
configuration space and later it's

130
00:06:30,840 --> 00:06:36,419
responsibility of the either BIOS or the

131
00:06:33,360 --> 00:06:39,479
operating system to set up the MMIO

132
00:06:36,419 --> 00:06:43,979
ranges so in a typical operating system

133
00:06:39,479 --> 00:06:46,860
firmware or BIOS queries all the PCI

134
00:06:43,979 --> 00:06:49,919
buses at the start time at the startup

135
00:06:46,860 --> 00:06:53,880
time by using PCI configuration space

136
00:06:49,919 --> 00:06:55,919
and find the devices present and also

137
00:06:53,880 --> 00:06:58,979
configure the system resources like

138
00:06:55,919 --> 00:07:02,400
memory space iOS space interrupt lines

139
00:06:58,979 --> 00:07:05,220
and Etc then it allocates the resource

140
00:07:02,400 --> 00:07:07,800
and tells the device about its

141
00:07:05,220 --> 00:07:10,620
allocation so in general PCA

142
00:07:07,800 --> 00:07:13,380
configurations was also contains a small

143
00:07:10,620 --> 00:07:15,539
amount of device type information helps

144
00:07:13,380 --> 00:07:18,780
the operating system to choose the

145
00:07:15,539 --> 00:07:22,740
appropriate device device driver for it

146
00:07:18,780 --> 00:07:26,099
and also as mm I always preferred over

147
00:07:22,740 --> 00:07:28,620
PMIO as we mentioned it before and the

148
00:07:26,099 --> 00:07:31,919
reason for that is because the

149
00:07:28,620 --> 00:07:34,740
instructions that perform portmap IO are

150
00:07:31,919 --> 00:07:38,580
limited to just one instructions you can

151
00:07:34,740 --> 00:07:41,039
all you can only have Eis ax or Al or

152
00:07:38,580 --> 00:07:43,919
just one register for this PMIO

153
00:07:41,039 --> 00:07:47,520
instructions and download registers that

154
00:07:43,919 --> 00:07:50,160
the data can be moved in and out is a

155
00:07:47,520 --> 00:07:53,539
bite size immediate value in the

156
00:07:50,160 --> 00:07:57,300
instruction or a value that is

157
00:07:53,539 --> 00:08:00,240
in this register which determines which

158
00:07:57,300 --> 00:08:03,240
port is the source or the destination

159
00:08:00,240 --> 00:08:05,819
force for this PMIO instruction to

160
00:08:03,240 --> 00:08:09,300
transfer the data and all of the general

161
00:08:05,819 --> 00:08:13,380
purpose registers can can be used to

162
00:08:09,300 --> 00:08:16,080
receive or send data in front for on the

163
00:08:13,380 --> 00:08:18,240
other hand because in memory map

164
00:08:16,080 --> 00:08:21,720
title all of the general purpose

165
00:08:18,240 --> 00:08:25,080
registers can be used for sending a

166
00:08:21,720 --> 00:08:27,660
receiving data from an into memory flow

167
00:08:25,080 --> 00:08:30,840
a memory mapped IO uses fewer

168
00:08:27,660 --> 00:08:34,800
instructions and and ultimately you can

169
00:08:30,840 --> 00:08:38,039
run faster than IO or PMIO if you want

170
00:08:34,800 --> 00:08:41,520
to just perform some IO dividing with

171
00:08:38,039 --> 00:08:44,159
MMIO devices you can use the old monitor

172
00:08:41,520 --> 00:08:47,160
command which is responsible for

173
00:08:44,159 --> 00:08:49,560
handling IoT bugging for MMIO devices

174
00:08:47,160 --> 00:08:52,680
and also if you want to just see the

175
00:08:49,560 --> 00:08:55,500
base address for different devices or

176
00:08:52,680 --> 00:08:58,260
the base physical address for different

177
00:08:55,500 --> 00:09:00,360
devices or where the device addresses

178
00:08:58,260 --> 00:09:02,399
mapped into the memory can there are

179
00:09:00,360 --> 00:09:05,640
also some resources I will try to

180
00:09:02,399 --> 00:09:08,459
introduce some of these devices later in

181
00:09:05,640 --> 00:09:10,440
this slides but generally we can see

182
00:09:08,459 --> 00:09:13,620
that for example this device is

183
00:09:10,440 --> 00:09:16,080
allocated and the mapped to the address

184
00:09:13,620 --> 00:09:20,279
shown in the picture now let's compare

185
00:09:16,080 --> 00:09:23,100
PMIO and MMIO devices for PMIO or

186
00:09:20,279 --> 00:09:25,920
isolated IO different addresses spaces

187
00:09:23,100 --> 00:09:28,380
or use for computer memory and electric

188
00:09:25,920 --> 00:09:31,920
device and devices have a dedicated

189
00:09:28,380 --> 00:09:35,339
address space while in a memory mapped

190
00:09:31,920 --> 00:09:38,519
IO or MMIO the same address is used for

191
00:09:35,339 --> 00:09:41,940
post memory and IO devices in case of

192
00:09:38,519 --> 00:09:45,060
PMIO separate con control units and

193
00:09:41,940 --> 00:09:49,920
control instructions are used in case of

194
00:09:45,060 --> 00:09:52,380
IO devices and in MMIO control units

195
00:09:49,920 --> 00:09:55,620
and instructions are same for memory and

196
00:09:52,380 --> 00:09:58,980
IO devices PMIO has a more complex and

197
00:09:55,620 --> 00:10:03,540
costlier than memory map dial as more

198
00:09:58,980 --> 00:10:05,940
passing these are needed by in MMIO it's

199
00:10:03,540 --> 00:10:09,300
easier to build and cheaper and cheap

200
00:10:05,940 --> 00:10:12,660
because it's less complex the entire

201
00:10:09,300 --> 00:10:15,120
address space can be used by memory as

202
00:10:12,660 --> 00:10:18,839
IO devices have separate I address

203
00:10:15,120 --> 00:10:21,959
spaces in PMIO while in MMIO some

204
00:10:18,839 --> 00:10:24,420
parts of other space of computer memory

205
00:10:21,959 --> 00:10:27,000
is consumed by IO devices as address

206
00:10:24,420 --> 00:10:29,519
space is shared or the PMIO computer

207
00:10:27,000 --> 00:10:31,920
memory and IO devices use different

208
00:10:29,519 --> 00:10:34,980
control instructions for read and write

209
00:10:31,920 --> 00:10:38,339
for example in or out instructions are

210
00:10:34,980 --> 00:10:41,459
used while in MMIO computer memory and I

211
00:10:38,339 --> 00:10:44,399
O devices can both use the same set of

212
00:10:41,459 --> 00:10:46,440
read and write instructions then use

213
00:10:44,399 --> 00:10:48,839
move instructions for example separate

214
00:10:46,440 --> 00:10:52,920
control bus is used for the computer

215
00:10:48,839 --> 00:10:56,399
memory and IO devices for PMIO in the

216
00:10:52,920 --> 00:10:59,760
same address and data bus are used and

217
00:10:56,399 --> 00:11:03,000
while in MMIO address data and Central

218
00:10:59,760 --> 00:11:07,640
bus are same for memory and IO devices

219
00:11:03,000 --> 00:11:12,000
now let's see an example of how we can

220
00:11:07,640 --> 00:11:16,680
analyze and modify the codes relating to

221
00:11:12,000 --> 00:11:21,120
a PC PS2 keyboard actually PS2 keyboards

222
00:11:16,680 --> 00:11:24,060
are some old versions of keyboards that

223
00:11:21,120 --> 00:11:26,339
are currently not used but I think it's

224
00:11:24,060 --> 00:11:28,980
it would be really good for an example

225
00:11:26,339 --> 00:11:33,120
because it's simple the protocol is

226
00:11:28,980 --> 00:11:35,220
simple and easy to understand and we

227
00:11:33,120 --> 00:11:37,620
can go after that we could extend our

228
00:11:35,220 --> 00:11:42,000
knowledge to perform the debugging of

229
00:11:37,620 --> 00:11:45,720
more complex IO devices so let's just

230
00:11:42,000 --> 00:11:48,500
notice that in most of the VMware

231
00:11:45,720 --> 00:11:53,100
versions they use

232
00:11:48,500 --> 00:11:55,260
they use a PS2 keyboard

233
00:11:53,100 --> 00:11:57,360
they still use but for the emulation

234
00:11:55,260 --> 00:12:00,320
they use PS3 keyboard

235
00:11:57,360 --> 00:12:03,740
so let's again

236
00:12:00,320 --> 00:12:03,740
turn on

237
00:12:03,779 --> 00:12:10,040
the WinDbg in order to disable the

238
00:12:06,600 --> 00:12:10,040
driver signature enforcement

239
00:12:10,140 --> 00:12:16,880
and

240
00:12:12,240 --> 00:12:16,880
see the VMware worker station

241
00:12:22,519 --> 00:12:29,839
 in VMware Workstation we could also

242
00:12:26,459 --> 00:12:29,839
use system information

243
00:12:35,120 --> 00:12:41,700
system information

244
00:12:38,399 --> 00:12:45,180
in hardware resources there are irq

245
00:12:41,700 --> 00:12:50,100
lines and IO devices as you can see

246
00:12:45,180 --> 00:12:52,820
here irq1 is used for the standard PS3

247
00:12:50,100 --> 00:12:52,820
keyboards

248
00:12:53,040 --> 00:12:59,600
and if we go to the

249
00:12:55,740 --> 00:13:02,880
eye or resources

250
00:12:59,600 --> 00:13:04,680
 we could see different IO

251
00:13:02,880 --> 00:13:08,100
resources that are used for the system

252
00:13:04,680 --> 00:13:09,360
and one of them is of course PS2

253
00:13:08,100 --> 00:13:14,399
keyboard

254
00:13:09,360 --> 00:13:19,639
which are which is available on this

255
00:13:14,399 --> 00:13:24,720
special port 60 port in hex format and

256
00:13:19,639 --> 00:13:28,820
64 in also enhance formats so we could

257
00:13:24,720 --> 00:13:33,480
use it as a starting point to see how

258
00:13:28,820 --> 00:13:36,540
these PS2 keyboards work so I just

259
00:13:33,480 --> 00:13:38,720
try to connect to HyperDbg I range from

260
00:13:36,540 --> 00:13:43,980
the source code

261
00:13:38,720 --> 00:13:43,980
 perform debugging

262
00:13:45,839 --> 00:13:49,519
connecting to the serial port

263
00:13:51,839 --> 00:13:56,000
this time I use debug version

264
00:14:08,160 --> 00:14:12,480


265
00:14:08,940 --> 00:14:15,320
well I know after performing this

266
00:14:12,480 --> 00:14:20,339
synchronization I just try to

267
00:14:15,320 --> 00:14:24,120
monitor monitor IO monitor this

268
00:14:20,339 --> 00:14:26,180
address 16 hex format to see what

269
00:14:24,120 --> 00:14:30,779
happens

270
00:14:26,180 --> 00:14:33,420
so I use if you see the help of IO

271
00:14:30,779 --> 00:14:37,440
in command

272
00:14:33,420 --> 00:14:39,240
it gets the port address and process ID

273
00:14:37,440 --> 00:14:41,100
and different items related to the

274
00:14:39,240 --> 00:14:46,560
events but we're only interested in port

275
00:14:41,100 --> 00:14:49,320
address and so I'll try to run IO in in

276
00:14:46,560 --> 00:14:53,100
60 port address in hex format everything

277
00:14:49,320 --> 00:14:57,060
in hybrid which is X format

278
00:14:53,100 --> 00:15:02,880
I'll continue the debuggee and now

279
00:14:57,060 --> 00:15:05,820
perform some pressing some keys here

280
00:15:02,880 --> 00:15:12,019
and everything just halts so let's just

281
00:15:05,820 --> 00:15:12,019
come back to the VM I will put it here

282
00:15:18,199 --> 00:15:28,199
so as we can see this address is able

283
00:15:23,399 --> 00:15:31,380
to read data from the keyboard it's

284
00:15:28,199 --> 00:15:36,240
in bus driver and this is the

285
00:15:31,380 --> 00:15:40,380
function so let's see this function

286
00:15:36,240 --> 00:15:43,199
by using the U command

287
00:15:40,380 --> 00:15:46,320
we want to disassemble this address and

288
00:15:43,199 --> 00:15:49,040
as we can see here it seems to be a

289
00:15:46,320 --> 00:15:53,339
simple function that only performs

290
00:15:49,040 --> 00:15:56,480
 in a simple return after in

291
00:15:53,339 --> 00:15:58,199
instruction is executed so

292
00:15:56,480 --> 00:16:01,860


293
00:15:58,199 --> 00:16:03,540
this is the address this is the address

294
00:16:01,860 --> 00:16:06,959
that we we are interested in

295
00:16:03,540 --> 00:16:11,760
intercepting and we have several options

296
00:16:06,959 --> 00:16:15,120
here we could intercept interrupts or

297
00:16:11,760 --> 00:16:18,360
for example or also we can also use

298
00:16:15,120 --> 00:16:20,760
any instruction to just modify the

299
00:16:18,360 --> 00:16:24,720
results of any instruction

300
00:16:20,760 --> 00:16:27,899
there's also another option which might

301
00:16:24,720 --> 00:16:32,339
be also which might be a better option

302
00:16:27,899 --> 00:16:34,260
to just put a simple EPT hook on this

303
00:16:32,339 --> 00:16:37,199
address because after running this

304
00:16:34,260 --> 00:16:39,000
instruction then when it when the

305
00:16:37,199 --> 00:16:42,959
processor wants to execute the next

306
00:16:39,000 --> 00:16:45,800
instructions already has the value of

307
00:16:42,959 --> 00:16:51,360
the input to the keyword

308
00:16:45,800 --> 00:16:55,920
 both of these ways can be used

309
00:16:51,360 --> 00:17:01,339
now let's use the second option here

310
00:16:55,920 --> 00:17:03,839
I already provide some

311
00:17:01,339 --> 00:17:06,600
resources here and the route test

312
00:17:03,839 --> 00:17:09,439
scripts you can see the script on the

313
00:17:06,600 --> 00:17:09,439
tutorial

314
00:17:11,480 --> 00:17:16,860
this this is a really good resource

315
00:17:14,459 --> 00:17:18,559
about it explaining different interrupts

316
00:17:16,860 --> 00:17:22,020
irq lines

317
00:17:18,559 --> 00:17:24,839
I think it's better to read it if you

318
00:17:22,020 --> 00:17:29,220
you want to get familiar with PS2

319
00:17:24,839 --> 00:17:32,640
keywords and the how general interrupt

320
00:17:29,220 --> 00:17:36,120
lines work on x86 processor also another

321
00:17:32,640 --> 00:17:42,419
resource which explains directly about

322
00:17:36,120 --> 00:17:45,860
PS3 keyboards is this it also provides

323
00:17:42,419 --> 00:17:50,220
information about different

324
00:17:45,860 --> 00:17:52,440
code sets or scan codes sets of these

325
00:17:50,220 --> 00:17:57,360
PS2 keyboards

326
00:17:52,440 --> 00:18:00,960
so I just try to put a simple EPT hook

327
00:17:57,360 --> 00:18:02,360
on this address because after this

328
00:18:00,960 --> 00:18:05,160
address Al

329
00:18:02,360 --> 00:18:08,220
contains the scan code

330
00:18:05,160 --> 00:18:09,919
and there are two scan codes for PS2

331
00:18:08,220 --> 00:18:13,620
keywords

332
00:18:09,919 --> 00:18:18,059
most of them are mentioned here

333
00:18:13,620 --> 00:18:20,520
but VMware Workstation uses the first or

334
00:18:18,059 --> 00:18:23,600
emulates the keyboard in the first scan

335
00:18:20,520 --> 00:18:23,600
code set so

336
00:18:24,200 --> 00:18:32,240
 we simply write a simple script to

337
00:18:29,100 --> 00:18:32,240
just print them

338
00:18:32,460 --> 00:18:35,460


339
00:18:36,720 --> 00:18:40,820
code or print the

340
00:18:42,780 --> 00:18:49,020
different keys that are pressed but also

341
00:18:45,660 --> 00:18:52,700
it's it's somehow like a simple key

342
00:18:49,020 --> 00:18:57,000
lagger hypervisor based key lager

343
00:18:52,700 --> 00:19:01,520
that is notified about the about

344
00:18:57,000 --> 00:19:01,520
pressing off a key before

345
00:19:01,740 --> 00:19:09,140
even the operating system is notified so

346
00:19:05,880 --> 00:19:09,140
I just use

347
00:19:13,020 --> 00:19:18,860
this simple printf here and print Al

348
00:19:16,200 --> 00:19:18,860
instruction

349
00:19:27,660 --> 00:19:35,100
before that I try to clear all

350
00:19:31,980 --> 00:19:35,100
 events

351
00:19:42,960 --> 00:19:51,299
I know I run the target debuggee and

352
00:19:49,020 --> 00:19:55,100
try to

353
00:19:51,299 --> 00:19:55,100
press some keys here

354
00:19:56,700 --> 00:20:05,340
like hello as we can see

355
00:20:00,380 --> 00:20:11,539
by entering any keys we could see how

356
00:20:05,340 --> 00:20:11,539
it's pressed and see the key code here

357
00:20:12,320 --> 00:20:19,820
 no no I

358
00:20:16,559 --> 00:20:22,799
just try to this this can be a simple

359
00:20:19,820 --> 00:20:25,320
hypervisor based key lager I try to just

360
00:20:22,799 --> 00:20:28,820


361
00:20:25,320 --> 00:20:28,820
clear all

362
00:20:30,020 --> 00:20:40,440
events and now let's just try to

363
00:20:33,900 --> 00:20:40,440
manipulate it by for example 

364
00:20:41,700 --> 00:20:45,600
when

365
00:20:44,520 --> 00:20:47,520


366
00:20:45,600 --> 00:20:52,740
let's see

367
00:20:47,520 --> 00:20:57,919
when Q or I don't know maybe when

368
00:20:52,740 --> 00:20:57,919
a scan code one for example when

369
00:21:00,539 --> 00:21:10,640
D when D is pressed then

370
00:21:04,280 --> 00:21:10,640
 the scan code for it would be 0 is 20

371
00:21:10,860 --> 00:21:21,720
so I'll try to modify it to another code

372
00:21:17,720 --> 00:21:23,160
we just show that a key is pressed the

373
00:21:21,720 --> 00:21:26,160
simple if

374
00:21:23,160 --> 00:21:26,160
statement

375
00:21:26,460 --> 00:21:33,960
when Al equals 2 0 it's

376
00:21:31,080 --> 00:21:38,760
20

377
00:21:33,960 --> 00:21:41,280
then we will change the AL to

378
00:21:38,760 --> 00:21:42,539
amount for example J we will change it

379
00:21:41,280 --> 00:21:46,679
to J

380
00:21:42,539 --> 00:21:51,000
 it's 24.

381
00:21:46,679 --> 00:21:53,100
so let's also there are also I I don't

382
00:21:51,000 --> 00:21:57,120
want to explain this particle in detail

383
00:21:53,100 --> 00:22:01,820
but there's all there is a

384
00:21:57,120 --> 00:22:05,640
press key and after that a release key

385
00:22:01,820 --> 00:22:08,240
is sent so we'll also try to change the

386
00:22:05,640 --> 00:22:08,240
release key

387
00:22:08,340 --> 00:22:12,679
so if D is

388
00:22:13,159 --> 00:22:22,400
released yeah

389
00:22:16,500 --> 00:22:22,400
for the first scan code then 0x a

390
00:22:22,440 --> 00:22:25,220
percent

391
00:22:26,520 --> 00:22:31,740
so if

392
00:22:28,039 --> 00:22:38,720
zero if the key code is equals to zero

393
00:22:31,740 --> 00:22:38,720
it's a 0 then it will change it to

394
00:22:40,320 --> 00:22:47,940
so 0x A4 because we want to change the D

395
00:22:45,720 --> 00:22:49,740
with j

396
00:22:47,940 --> 00:22:55,380


397
00:22:49,740 --> 00:22:59,640
0 x A4 yeah that should be fine

398
00:22:55,380 --> 00:23:00,659
also that's print

399
00:22:59,640 --> 00:23:04,400
some

400
00:23:00,659 --> 00:23:04,400
keys here like

401
00:23:04,640 --> 00:23:10,580
he is

402
00:23:08,100 --> 00:23:10,580
press

403
00:23:13,799 --> 00:23:20,220
now let's run this script

404
00:23:16,580 --> 00:23:22,860
I try to just because we are

405
00:23:20,220 --> 00:23:26,820
using the same snapshot I think is

406
00:23:22,860 --> 00:23:29,780
better to rerun and this snapshot but

407
00:23:26,820 --> 00:23:34,020
anyway let's just try to

408
00:23:29,780 --> 00:23:37,340
return to the controller here I try to

409
00:23:34,020 --> 00:23:37,340
run the snapshot

410
00:23:44,220 --> 00:23:50,419
again connect it because because the

411
00:23:46,919 --> 00:23:50,419
addresses are not changed here

412
00:23:59,159 --> 00:24:01,880
foreign

413
00:24:27,780 --> 00:24:32,340
just waiting because my individually

414
00:24:30,480 --> 00:24:35,780
wants to connect

415
00:24:32,340 --> 00:24:39,539
and when it's connected

416
00:24:35,780 --> 00:24:42,000
I will try again and now there's some

417
00:24:39,539 --> 00:24:45,500
transitions performed so let's just copy

418
00:24:42,000 --> 00:24:45,500
the same script

419
00:24:57,080 --> 00:25:03,020
as we can see here when a is pressed

420
00:25:00,179 --> 00:25:07,080
then we will see how the key press

421
00:25:03,020 --> 00:25:08,640
instructions here but if anyway I press

422
00:25:07,080 --> 00:25:11,520
d

423
00:25:08,640 --> 00:25:14,220
then it's changed then it's changed to J

424
00:25:11,520 --> 00:25:17,580
like I'm currently pressing D and you

425
00:25:14,220 --> 00:25:23,220
can see that it's depressed but 

426
00:25:17,580 --> 00:25:27,679
meanwhile G is shown here

427
00:25:23,220 --> 00:25:27,679
and J is shown here excuse me

428
00:25:27,720 --> 00:25:32,940


429
00:25:29,520 --> 00:25:35,220
and if I press J

430
00:25:32,940 --> 00:25:39,860
then it's just like a regular

431
00:25:35,220 --> 00:25:39,860
instruction but when I press e here

432
00:25:40,700 --> 00:25:45,799
 it changes to J

433
00:25:46,940 --> 00:25:55,500
so this was a simple example of how we

434
00:25:51,600 --> 00:25:58,799
can use hybrid imagery to use to debug

435
00:25:55,500 --> 00:26:01,320
different IO devices we could extend

436
00:25:58,799 --> 00:26:04,580
our knowledge and combine it with SQL

437
00:26:01,320 --> 00:26:07,919
engine to perform more complex scenarios

438
00:26:04,580 --> 00:26:10,260
this was just a simple case that shows

439
00:26:07,919 --> 00:26:13,440
how you can perform key lagging by a

440
00:26:10,260 --> 00:26:18,179
hypervisor even modifying the resources

441
00:26:13,440 --> 00:26:20,700
the physical resources in summary and in

442
00:26:18,179 --> 00:26:24,120
this section we see how external

443
00:26:20,700 --> 00:26:26,340
interrupts are used as a way to

444
00:26:24,120 --> 00:26:29,220
communicate to notify the processor

445
00:26:26,340 --> 00:26:31,799
about different events and we will see

446
00:26:29,220 --> 00:26:34,679
we also saw different ways of

447
00:26:31,799 --> 00:26:37,260
communication between profiles like PMIO

448
00:26:34,679 --> 00:26:39,900
and MMIO and the differences between

449
00:26:37,260 --> 00:26:43,860
these two methods and finally in the

450
00:26:39,900 --> 00:26:46,799
hands-on section we monitor and

451
00:26:43,860 --> 00:26:49,919
sports for PS2 keyboards and wrote a

452
00:26:46,799 --> 00:26:52,440
simple script to change some of the keys

453
00:26:49,919 --> 00:26:55,679
in the keyword before finishing as I

454
00:26:52,440 --> 00:26:59,100
mentioned before some tools here that

455
00:26:55,679 --> 00:27:01,140
used to show the base address for MMIO

456
00:26:59,100 --> 00:27:04,140
devices the tools that are used in

457
00:27:01,140 --> 00:27:08,820
devices HW info also some of the

458
00:27:04,140 --> 00:27:12,539
descriptions derived from this source

459
00:27:08,820 --> 00:27:15,620
nothing more thanks for watching and see

460
00:27:12,539 --> 00:27:15,620
you in the next section

