1
00:00:00,319 --> 00:00:06,600
let's see how we can customize the

2
00:00:03,600 --> 00:00:07,820
settings of the HyperDbg there there

3
00:00:06,600 --> 00:00:11,219
are some

4
00:00:07,820 --> 00:00:13,799
customization for settings one of

5
00:00:11,219 --> 00:00:16,139
them if we want to just query or change

6
00:00:13,799 --> 00:00:17,699
each of them we could use that so we

7
00:00:16,139 --> 00:00:22,260
could use that setting or settings

8
00:00:17,699 --> 00:00:25,500
command and it will if we just use the

9
00:00:22,260 --> 00:00:28,400
feature and without any extra

10
00:00:25,500 --> 00:00:31,439
argument then it's just queries the

11
00:00:28,400 --> 00:00:35,640
state of that feature for example Ultron

12
00:00:31,439 --> 00:00:39,180
pass is enabled by default if we want to

13
00:00:35,640 --> 00:00:42,960
just turn off the Ultron pause then we

14
00:00:39,180 --> 00:00:44,760
could just switch on pause off or on all

15
00:00:42,960 --> 00:00:49,460
of them are possible through the

16
00:00:44,760 --> 00:00:49,460
settings command let's see

17
00:00:55,620 --> 00:01:01,399
currently these features are available

18
00:00:58,260 --> 00:01:05,460
for example if you want to change the

19
00:01:01,399 --> 00:01:08,880
syntax of the assembler we could use

20
00:01:05,460 --> 00:01:11,460
this command by default is Intel syntax

21
00:01:08,880 --> 00:01:16,740
if we want to use

22
00:01:11,460 --> 00:01:19,439
syntax of atnt or master style syntax

23
00:01:16,740 --> 00:01:21,920
then we could change it by using these

24
00:01:19,439 --> 00:01:21,920
commands

