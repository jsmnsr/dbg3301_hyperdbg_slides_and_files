1
00:00:00,000 --> 00:00:06,620
the next section here is bringing pages

2
00:00:03,540 --> 00:00:10,920
into the memory or into the wrap

3
00:00:06,620 --> 00:00:15,120
accessing memory in HyperDbg might

4
00:00:10,920 --> 00:00:19,680
sometimes show that the address is

5
00:00:15,120 --> 00:00:22,199
on available it's unvalid but we're sure

6
00:00:19,680 --> 00:00:24,720
that the target address is valid

7
00:00:22,199 --> 00:00:26,359
especially when we use the Dodge start

8
00:00:24,720 --> 00:00:31,439
command

9
00:00:26,359 --> 00:00:37,079
 this is mainly this is because

10
00:00:31,439 --> 00:00:39,899
 mainly Windows don't make every

11
00:00:37,079 --> 00:00:42,360
page a table available to the pro to the

12
00:00:39,899 --> 00:00:46,860
process like for example some of the

13
00:00:42,360 --> 00:00:49,260
functions might might be valid and might

14
00:00:46,860 --> 00:00:52,980
be even available in the memory but

15
00:00:49,260 --> 00:00:55,680
Windows might not make it available into

16
00:00:52,980 --> 00:00:59,280
the target page tables of the target

17
00:00:55,680 --> 00:01:04,559
process and as we are viewing the memory

18
00:00:59,280 --> 00:01:08,700
from from the CR CR3 of the target

19
00:01:04,559 --> 00:01:11,460
process so we might miss miss it but

20
00:01:08,700 --> 00:01:14,880
for example we might not notice that

21
00:01:11,460 --> 00:01:18,840
it's not that is available and we

22
00:01:14,880 --> 00:01:21,479
have to let the Windows page manager to

23
00:01:18,840 --> 00:01:25,380
bring the page into the memory

24
00:01:21,479 --> 00:01:28,380
yes in these cases if we want to bring

25
00:01:25,380 --> 00:01:31,520
the page into the memory we can use the

26
00:01:28,380 --> 00:01:34,320
dot page in command

27
00:01:31,520 --> 00:01:36,960
but of course we should be cautious

28
00:01:34,320 --> 00:01:40,259
because the incorrect use of this

29
00:01:36,960 --> 00:01:43,820
command is like injecting or or in other

30
00:01:40,259 --> 00:01:47,759
words like accessing and invalid 

31
00:01:43,820 --> 00:01:49,860
memory address so it might result an

32
00:01:47,759 --> 00:01:52,880
exception and the exception might be

33
00:01:49,860 --> 00:01:55,619
handled by for example the SCH handler

34
00:01:52,880 --> 00:02:00,000
sometimes in the kernel it might cause

35
00:01:55,619 --> 00:02:03,360
blue aircraft that or in user mode

36
00:02:00,000 --> 00:02:07,680
processes it might cost crash so we have

37
00:02:03,360 --> 00:02:11,400
to keep in mind that how we how we

38
00:02:07,680 --> 00:02:15,560
want to use this command and whether or

39
00:02:11,400 --> 00:02:19,739
target address is valid or not

40
00:02:15,560 --> 00:02:22,620
 with this that page in command all

41
00:02:19,739 --> 00:02:27,060
the processes and all the target threads

42
00:02:22,620 --> 00:02:30,120
 resume but HyperDbg set a trap flag

43
00:02:27,060 --> 00:02:32,580
in the target target thread and only one

44
00:02:30,120 --> 00:02:36,180
instruction in the target process will

45
00:02:32,580 --> 00:02:38,220
be executed so for example we could use

46
00:02:36,180 --> 00:02:41,700
some commands like that page in without

47
00:02:38,220 --> 00:02:42,840
any parameters specify a function

48
00:02:41,700 --> 00:02:46,620
address

49
00:02:42,840 --> 00:02:49,620
 or we could use that paging with some

50
00:02:46,620 --> 00:02:53,760
 with some error codes we can specify

51
00:02:49,620 --> 00:02:57,720
the error codes for the page Vault 

52
00:02:53,760 --> 00:03:01,560
for example UF in this example I I made

53
00:02:57,720 --> 00:03:04,200
a list of common error codes that

54
00:03:01,560 --> 00:03:06,360
might be used with this command there

55
00:03:04,200 --> 00:03:09,239
are also other codes that might be used

56
00:03:06,360 --> 00:03:11,879
as low but these are the common error

57
00:03:09,239 --> 00:03:17,040
codes for example the first one is

58
00:03:11,879 --> 00:03:19,800
like a page not font  W if you

59
00:03:17,040 --> 00:03:23,300
specify W instead of UF for example in

60
00:03:19,800 --> 00:03:26,819
the previous example it it

61
00:03:23,300 --> 00:03:29,580
implies once it's injected into the

62
00:03:26,819 --> 00:03:33,780
guest it implies that there were there

63
00:03:29,580 --> 00:03:37,019
was a right access fault or PW means

64
00:03:33,780 --> 00:03:39,599
that the page is not present and the

65
00:03:37,019 --> 00:03:43,440
access was a right access fault or for

66
00:03:39,599 --> 00:03:46,680
example UF which which is shown in

67
00:03:43,440 --> 00:03:50,459
the previous slide means that a user

68
00:03:46,680 --> 00:03:53,599
address instruction fetch fault happens

69
00:03:50,459 --> 00:03:58,260
into the target process

70
00:03:53,599 --> 00:04:01,920
 to demonstrate how we can use this

71
00:03:58,260 --> 00:04:04,560
command I made a simple application and

72
00:04:01,920 --> 00:04:08,159
this is a really simple message back a

73
00:04:04,560 --> 00:04:14,159
message box application you can find its

74
00:04:08,159 --> 00:04:16,040
source code into the files beside the

75
00:04:14,159 --> 00:04:22,260
git repo

76
00:04:16,040 --> 00:04:27,120
 it's an x86 application I I

77
00:04:22,260 --> 00:04:31,199
simply run it into in into the my into

78
00:04:27,120 --> 00:04:35,540
my target system so just shows a simple

79
00:04:31,199 --> 00:04:35,540
message box hello HyperDbg

80
00:04:35,600 --> 00:04:41,880
 if and there's nothing special about

81
00:04:39,060 --> 00:04:44,639
this message box it's a simple text

82
00:04:41,880 --> 00:04:48,080
message box I know if we want to run it

83
00:04:44,639 --> 00:04:52,340
by using the data start command

84
00:04:48,080 --> 00:04:52,340
 your

85
00:04:58,919 --> 00:05:07,620
 once we reach to the target

86
00:05:02,780 --> 00:05:09,600
applications and through 

87
00:05:07,620 --> 00:05:14,340
entry point

88
00:05:09,600 --> 00:05:16,500
 we could see that if we want to

89
00:05:14,340 --> 00:05:21,120
put for example breakpoint and the

90
00:05:16,500 --> 00:05:23,160
message box  the address is not

91
00:05:21,120 --> 00:05:28,080
valid so for example

92
00:05:23,160 --> 00:05:31,259
 message box is located in 

93
00:05:28,080 --> 00:05:35,280
users through dll and we are calling

94
00:05:31,259 --> 00:05:37,440
message box a but it said that they

95
00:05:35,280 --> 00:05:40,940
couldn't resolve a result error at this

96
00:05:37,440 --> 00:05:44,940
address let's see whether I set the

97
00:05:40,940 --> 00:05:50,160
symbol pass yes it's present so I just

98
00:05:44,940 --> 00:05:50,160
have I have to reload the 

99
00:05:50,180 --> 00:05:57,240
 symbols for this special process if

100
00:05:54,660 --> 00:06:00,660
we want to see it's marginal

101
00:05:57,240 --> 00:06:03,180
addresses we could use this function but

102
00:06:00,660 --> 00:06:06,300
keep in mind that this is a 32-bit

103
00:06:03,180 --> 00:06:10,560
application so we need to reload the

104
00:06:06,300 --> 00:06:13,680
symbol for this specific application and

105
00:06:10,560 --> 00:06:14,940
here we can see that the user32 is also

106
00:06:13,680 --> 00:06:20,340
loaded

107
00:06:14,940 --> 00:06:23,280
 let's reload the symbols for

108
00:06:20,340 --> 00:06:25,520
this application

109
00:06:23,280 --> 00:06:25,520
foreign

110
00:06:32,720 --> 00:06:42,780
once it's done we could again

111
00:06:36,120 --> 00:06:42,780
access users to 

112
00:06:43,139 --> 00:06:49,380
message which would be but before that

113
00:06:45,600 --> 00:06:54,000
let's see it in in the debugger we see

114
00:06:49,380 --> 00:07:00,060
it here now I click OK I want to start

115
00:06:54,000 --> 00:07:03,840
the game the same  symbols as

116
00:07:00,060 --> 00:07:06,479
the previous loaded symbol is the

117
00:07:03,840 --> 00:07:08,960
remained into the system so I just 

118
00:07:06,479 --> 00:07:12,840
use the does start command

119
00:07:08,960 --> 00:07:16,259
 right into the entry point and again

120
00:07:12,840 --> 00:07:20,819
I put a message box here

121
00:07:16,259 --> 00:07:24,000
 I put a breakpoint I want to see

122
00:07:20,819 --> 00:07:25,139
 the memory disassemble the memory in

123
00:07:24,000 --> 00:07:27,599
the

124
00:07:25,139 --> 00:07:30,479
message box

125
00:07:27,599 --> 00:07:32,599
and as we can see here it says that the

126
00:07:30,479 --> 00:07:36,000
address is invalued

127
00:07:32,599 --> 00:07:38,880
but actually this address is valid but

128
00:07:36,000 --> 00:07:43,080
Windows didn't bring it or didn't page

129
00:07:38,880 --> 00:07:45,900
it in into the memory or even if it's 

130
00:07:43,080 --> 00:07:50,340
already paged into the memory the

131
00:07:45,900 --> 00:07:53,340
entries of the page table for this 

132
00:07:50,340 --> 00:07:58,440
process is still it won't show a valid

133
00:07:53,340 --> 00:08:01,919
address for this user module so I can

134
00:07:58,440 --> 00:08:03,900
simply use the Doc page in command most

135
00:08:01,919 --> 00:08:07,259
of the time you don't need to specify

136
00:08:03,900 --> 00:08:12,599
any other options only that paging

137
00:08:07,259 --> 00:08:14,720
without any argument works so I use a

138
00:08:12,599 --> 00:08:19,020
user to

139
00:08:14,720 --> 00:08:20,759
message box and it's not case

140
00:08:19,020 --> 00:08:24,120
sensitive

141
00:08:20,759 --> 00:08:26,819
so I run this command and said that the

142
00:08:24,120 --> 00:08:30,120
page file is delivered to the target now

143
00:08:26,819 --> 00:08:32,640
we have to press G and in one

144
00:08:30,120 --> 00:08:35,099
instruction or this jump instruction

145
00:08:32,640 --> 00:08:38,459
will be executed a trap black is

146
00:08:35,099 --> 00:08:41,599
automatically set in this process so I

147
00:08:38,459 --> 00:08:45,180
just press G and after that

148
00:08:41,599 --> 00:08:47,940
 we are we we execute one instruction

149
00:08:45,180 --> 00:08:51,779
we are here

150
00:08:47,940 --> 00:08:56,519
 no actually again if we want to check

151
00:08:51,779 --> 00:08:59,279
the message box a we can see that and

152
00:08:56,519 --> 00:09:00,180
now the address for this message box is

153
00:08:59,279 --> 00:09:03,180
value

154
00:09:00,180 --> 00:09:05,660
so we could easily put some breakpoints

155
00:09:03,180 --> 00:09:05,660
here

156
00:09:11,760 --> 00:09:17,339
and continue the debugging and as you

157
00:09:14,519 --> 00:09:22,440
can see the target breakpoint is

158
00:09:17,339 --> 00:09:28,680
executed and we are in the simple

159
00:09:22,440 --> 00:09:31,100
message box project and in this

160
00:09:28,680 --> 00:09:31,100
function

161
00:09:31,740 --> 00:09:40,760
 another thing to keep in mind is that

162
00:09:34,860 --> 00:09:44,940
once you use you sometimes this is a

163
00:09:40,760 --> 00:09:48,839
32-bit application so if we use U here

164
00:09:44,940 --> 00:09:50,760
 the debugger show us a warning

165
00:09:48,839 --> 00:09:54,600
message that the target seems to be

166
00:09:50,760 --> 00:09:59,459
located in a 32-bit program so in this

167
00:09:54,600 --> 00:10:04,279
case we have to use u32 to show the

168
00:09:59,459 --> 00:10:11,279
actual to use the 32-bit disassembler so

169
00:10:04,279 --> 00:10:11,279
instead of using you I use u32

170
00:10:12,260 --> 00:10:18,600
or you too

171
00:10:14,940 --> 00:10:21,380
both of them are the same and it shows

172
00:10:18,600 --> 00:10:25,200
the target disassembly in

173
00:10:21,380 --> 00:10:28,920
 32-bit format

174
00:10:25,200 --> 00:10:31,580
or the orbit the 32-bit that's similar

175
00:10:28,920 --> 00:10:31,580


176
00:10:32,279 --> 00:10:37,700
actually it is

177
00:10:34,680 --> 00:10:42,180
here yeah this this is the correct

178
00:10:37,700 --> 00:10:45,140
32-bit format of the

179
00:10:42,180 --> 00:10:45,140


180
00:10:46,079 --> 00:10:53,899
of the target function and 

181
00:10:50,459 --> 00:10:58,620
as you can see we are on 

182
00:10:53,899 --> 00:11:01,140
user32 a w version or row 64 version of

183
00:10:58,620 --> 00:11:04,019
user search

184
00:11:01,140 --> 00:11:06,440
yeah that's it let's go back to the

185
00:11:04,019 --> 00:11:06,440
slides

