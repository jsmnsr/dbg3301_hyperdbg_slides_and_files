1
00:00:00,000 --> 00:00:06,060
hi everyone welcome to the next part of

2
00:00:02,940 --> 00:00:08,099
the tutorial reversing with Hyper gpg in

3
00:00:06,060 --> 00:00:09,960
this part we're gonna talk about some

4
00:00:08,099 --> 00:00:12,900
advanced debugging techniques like

5
00:00:09,960 --> 00:00:17,520
hooking IVC and different instruction

6
00:00:12,900 --> 00:00:20,640
level cooking so here's a brief outline

7
00:00:17,520 --> 00:00:23,460
of this part we're gonna see we're

8
00:00:20,640 --> 00:00:26,100
gonna talk about interrupt descriptor

9
00:00:23,460 --> 00:00:28,800
table and all of the things that are

10
00:00:26,100 --> 00:00:31,439
related to false exception interrupts

11
00:00:28,800 --> 00:00:33,660
and we will see how we can monitor

12
00:00:31,439 --> 00:00:38,719
exception or external interrupts in

13
00:00:33,660 --> 00:00:42,180
HyperDbg and the next section is about

14
00:00:38,719 --> 00:00:45,059
MSR registers we will see how we can

15
00:00:42,180 --> 00:00:48,300
read and write into the MSR registers

16
00:00:45,059 --> 00:00:52,700
how we can monitor reads or write into

17
00:00:48,300 --> 00:00:56,100
these registers and we have a part about

18
00:00:52,700 --> 00:00:58,199
cpuid instruction and how we can hook

19
00:00:56,100 --> 00:01:02,660
this instruction along with other

20
00:00:58,199 --> 00:01:07,080
instructions like rdks see rdtscp and

21
00:01:02,660 --> 00:01:10,619
rdpmc also we'll see how we can hook

22
00:01:07,080 --> 00:01:12,619
hyper calls or VM calls and it is also

23
00:01:10,619 --> 00:01:16,740
possible to view

24
00:01:12,619 --> 00:01:19,860
the possible changes in a debug hardware

25
00:01:16,740 --> 00:01:22,439
debug registers and in the hands-on

26
00:01:19,860 --> 00:01:24,659
section we will see how we can inject

27
00:01:22,439 --> 00:01:28,799
codes into running threads and into

28
00:01:24,659 --> 00:01:32,520
running processes the first section is

29
00:01:28,799 --> 00:01:37,320
about false exceptions and interrupts as

30
00:01:32,520 --> 00:01:40,680
you probably know there's a table in x86

31
00:01:37,320 --> 00:01:44,700
processors called IDT table or intrap

32
00:01:40,680 --> 00:01:47,040
descriptor table this this is and this

33
00:01:44,700 --> 00:01:49,439
is an architectural implementation of

34
00:01:47,040 --> 00:01:53,579
interrupt vector table in retail

35
00:01:49,439 --> 00:01:56,399
processors and if there is there is a

36
00:01:53,579 --> 00:01:59,640
exception there's a fault or there is an

37
00:01:56,399 --> 00:02:03,060
interrupt in the processor then the

38
00:01:59,640 --> 00:02:05,880
process or will try to find the correct

39
00:02:03,060 --> 00:02:09,720
routine that is responsible for handling

40
00:02:05,880 --> 00:02:13,800
and this interrupt so it tries to look

41
00:02:09,720 --> 00:02:18,420
at IDT to find the correct response for

42
00:02:13,800 --> 00:02:21,360
the exceptional interrupts so IDT is

43
00:02:18,420 --> 00:02:23,940
generally triggered by three types of

44
00:02:21,360 --> 00:02:26,400
events hardware interrupt soft brain

45
00:02:23,940 --> 00:02:28,920
traps and processor exceptions

46
00:02:26,400 --> 00:02:32,640
them are referred as interrupt so

47
00:02:28,920 --> 00:02:35,160
basically if in case of HyperDbg in

48
00:02:32,640 --> 00:02:38,760
case of both processor and HyperDbg if

49
00:02:35,160 --> 00:02:42,680
we're gonna divide the interrupts and

50
00:02:38,760 --> 00:02:45,319
exceptions into two parts the IDC

51
00:02:42,680 --> 00:02:51,980
consists of

52
00:02:45,319 --> 00:02:57,239
256 interrupt vectors so the first 32 

53
00:02:51,980 --> 00:03:00,180
32 entries of IDT are used for processor

54
00:02:57,239 --> 00:03:04,680
exceptions and the remaining entries are

55
00:03:00,180 --> 00:03:07,980
used for an external device in drops if

56
00:03:04,680 --> 00:03:11,700
we want to monitor the first 32

57
00:03:07,980 --> 00:03:14,459
entries of IDT we could use a bang

58
00:03:11,700 --> 00:03:17,400
exception or exclamation mark exception

59
00:03:14,459 --> 00:03:19,440
event command even though it's

60
00:03:17,400 --> 00:03:22,260
possible but it's generally not a good

61
00:03:19,440 --> 00:03:25,019
idea to monitor all exceptions because

62
00:03:22,260 --> 00:03:28,980
it has a high rate of exceptions that

63
00:03:25,019 --> 00:03:31,500
coming into the system so it's not a

64
00:03:28,980 --> 00:03:36,540
good idea to intercept all of them 

65
00:03:31,500 --> 00:03:41,299
but you can specify which exception

66
00:03:36,540 --> 00:03:45,299
entry you want to monitor and 

67
00:03:41,299 --> 00:03:46,940
monitor that by using this exception

68
00:03:45,299 --> 00:03:51,659
event command

69
00:03:46,940 --> 00:03:57,299
 here is an example of intercepting

70
00:03:51,659 --> 00:04:00,299
 exception or 0xe exception for

71
00:03:57,299 --> 00:04:02,760
page faults don't worry about these

72
00:04:00,299 --> 00:04:06,420
stage post that we will get to it for

73
00:04:02,760 --> 00:04:09,480
now we just add it to make sure that the

74
00:04:06,420 --> 00:04:12,840
CR2 is valid I will explain it I will

75
00:04:09,480 --> 00:04:16,739
explain this stage post layer in this

76
00:04:12,840 --> 00:04:19,979
series but no let's just run this

77
00:04:16,739 --> 00:04:22,800
command into the high produce and see

78
00:04:19,979 --> 00:04:26,820
the page false coming into the system

79
00:04:22,800 --> 00:04:29,360
I already connected to this instance of

80
00:04:26,820 --> 00:04:32,820
HyperDbg

81
00:04:29,360 --> 00:04:39,560
 so I run this command

82
00:04:32,820 --> 00:04:39,560
and as you can see here we have 

83
00:04:40,020 --> 00:04:47,460
 different let's just run it again and

84
00:04:44,100 --> 00:04:51,240
pause it and as you can see we can 

85
00:04:47,460 --> 00:04:54,180
see different processes that page

86
00:04:51,240 --> 00:04:57,720
call happens into these processes at the

87
00:04:54,180 --> 00:05:01,680
page file or CR2 register is this 

88
00:04:57,720 --> 00:05:05,340
this address so probably some pages

89
00:05:01,680 --> 00:05:09,540
some pages happen to not be present

90
00:05:05,340 --> 00:05:13,020
in the memory or violation happens

91
00:05:09,540 --> 00:05:15,740
here or maybe page out into the and

92
00:05:13,020 --> 00:05:19,560
it's no longer available in the ramp

93
00:05:15,740 --> 00:05:24,360
so this page faults happened and this

94
00:05:19,560 --> 00:05:26,960
way we can intercept 0xe or page

95
00:05:24,360 --> 00:05:26,960
false

96
00:05:27,960 --> 00:05:36,960
foreign let's return back to a slides if

97
00:05:32,400 --> 00:05:41,639
you want to manage your entries that

98
00:05:36,960 --> 00:05:47,240
are related to entries after church

99
00:05:41,639 --> 00:05:54,840
to after first 32-bit entries in the IDT

100
00:05:47,240 --> 00:05:58,320
in this range from 33 to 256 we have to

101
00:05:54,840 --> 00:06:02,639
use the interrupt command in HyperDbg so

102
00:05:58,320 --> 00:06:06,479
in case if we want to monitor a special

103
00:06:02,639 --> 00:06:09,660
 for example interrupt handler we

104
00:06:06,479 --> 00:06:13,199
could use this command and

105
00:06:09,660 --> 00:06:17,039
it's generally advice again and and

106
00:06:13,199 --> 00:06:19,440
something not possible to monitor all

107
00:06:17,039 --> 00:06:23,220
the interrupts because the higher the

108
00:06:19,440 --> 00:06:25,680
rate of interrupts are really high but

109
00:06:23,220 --> 00:06:28,080
generally if you want to see some of

110
00:06:25,680 --> 00:06:32,160
these interrupts for example we we

111
00:06:28,080 --> 00:06:36,300
can see the interrupts for keyboard for

112
00:06:32,160 --> 00:06:39,539
other devices other external IO devices

113
00:06:36,300 --> 00:06:42,180
and even if the if there is a clock

114
00:06:39,539 --> 00:06:44,100
interrupt in the operating system I mean

115
00:06:42,180 --> 00:06:46,560
and we want to monitor the clock

116
00:06:44,100 --> 00:06:49,940
interrupt we could also use this command

117
00:06:46,560 --> 00:06:53,819
for example I made a list of available

118
00:06:49,940 --> 00:06:56,400
 some of the interrupt some of the

119
00:06:53,819 --> 00:07:00,060
interrupts that are used by Windows for

120
00:06:56,400 --> 00:07:02,580
example these are in decimal formats so

121
00:07:00,060 --> 00:07:04,759
if I want to simply convert them to a

122
00:07:02,580 --> 00:07:10,560
hexade Smart format

123
00:07:04,759 --> 00:07:14,699
 this one is for APC intro apps the

124
00:07:10,560 --> 00:07:17,520
DPC interrupt is located at 47

125
00:07:14,699 --> 00:07:22,380
and this is the interrupt that is used

126
00:07:17,520 --> 00:07:24,240
for clocking drops so if you want to

127
00:07:22,380 --> 00:07:27,660
see the clock interrupts we should

128
00:07:24,240 --> 00:07:31,940
monitor this address D1 in hexadecimal

129
00:07:27,660 --> 00:07:35,639
format and also there are inter

130
00:07:31,940 --> 00:07:37,800
processor interrupts API and PMI in

131
00:07:35,639 --> 00:07:40,560
transits that are located at these

132
00:07:37,800 --> 00:07:44,819
specific addresses

133
00:07:40,560 --> 00:07:47,120
so let's try to just monitor for example

134
00:07:44,819 --> 00:07:50,039
 interrupts

135
00:07:47,120 --> 00:07:56,479
that are related to the clock interrupt

136
00:07:50,039 --> 00:07:56,479
so I try to monitor d z D1 

137
00:07:56,880 --> 00:07:59,479
yeah

138
00:08:00,199 --> 00:08:06,419
 D1 is the same as clock interrupt

139
00:08:05,280 --> 00:08:12,800
here

140
00:08:06,419 --> 00:08:12,800
and in this script I try to write

141
00:08:17,400 --> 00:08:20,960
and print a function

142
00:08:23,060 --> 00:08:27,680
that monitors the core

143
00:08:39,779 --> 00:08:48,080
and

144
00:08:42,779 --> 00:08:48,080
the targets RIT instruction

145
00:08:50,580 --> 00:08:57,440
rip or eip instruction depends on the

146
00:08:54,959 --> 00:09:02,519
process

147
00:08:57,440 --> 00:09:04,380
and also the process name

148
00:09:02,519 --> 00:09:07,040
or process

149
00:09:04,380 --> 00:09:07,040
Eid

150
00:09:19,500 --> 00:09:24,540
okay let's run this code to see what

151
00:09:22,920 --> 00:09:27,540
happens

152
00:09:24,540 --> 00:09:27,540


153
00:09:29,240 --> 00:09:34,160
I'm not connected to the debugger

154
00:09:36,600 --> 00:09:44,279
again I try to connect my

155
00:09:40,200 --> 00:09:46,399
target VM machine

156
00:09:44,279 --> 00:09:46,399


157
00:09:46,980 --> 00:09:53,399
the VM is not connected to the ring dvg

158
00:09:50,160 --> 00:09:56,060
that's why it's not responding

159
00:09:53,399 --> 00:09:56,060
okay

160
00:10:17,760 --> 00:10:19,760


161
00:10:21,180 --> 00:10:27,779
so just a little bit see what happens in

162
00:10:25,080 --> 00:10:31,980
in the target Windows as you can see a

163
00:10:27,779 --> 00:10:34,080
different course receive interrupts 

164
00:10:31,980 --> 00:10:36,260
this this is a little bit change in in

165
00:10:34,080 --> 00:10:39,180
the recent version of the Windows in the

166
00:10:36,260 --> 00:10:42,959
past version of Windows as long as I

167
00:10:39,180 --> 00:10:46,560
know they broadcast the clock interrupts

168
00:10:42,959 --> 00:10:50,519
only on the zeroth core or only on

169
00:10:46,560 --> 00:10:52,560
the first core and then try to like

170
00:10:50,519 --> 00:10:55,320


171
00:10:52,560 --> 00:10:58,740
make a coordination between the course

172
00:10:55,320 --> 00:11:03,600
or synchronizing the course by using IPI

173
00:10:58,740 --> 00:11:06,180
inter processor interrupt but from

174
00:11:03,600 --> 00:11:09,300
what the results that we see in the

175
00:11:06,180 --> 00:11:11,940
current version of the Windows

176
00:11:09,300 --> 00:11:14,459
 currently they 

177
00:11:11,940 --> 00:11:17,820
send clock interrupts to all of the

178
00:11:14,459 --> 00:11:20,160
cores and we can we can see clearly here

179
00:11:17,820 --> 00:11:22,279
that all of the courts receive clock

180
00:11:20,160 --> 00:11:25,920
interrupts

181
00:11:22,279 --> 00:11:29,579
 probably it's because of some

182
00:11:25,920 --> 00:11:32,940
optimizations because this is a

183
00:11:29,579 --> 00:11:36,300
highly the critical part of the Windows

184
00:11:32,940 --> 00:11:40,079
it has a high rate of execution so maybe

185
00:11:36,300 --> 00:11:42,120
this is for optimization reasons or

186
00:11:40,079 --> 00:11:46,079
maybe other reasons I don't know but

187
00:11:42,120 --> 00:11:49,980
from what we see all the cores received

188
00:11:46,079 --> 00:11:53,959
the D1 or a clock interrupt so let's

189
00:11:49,980 --> 00:11:53,959
just try to clear all of them

190
00:11:54,060 --> 00:12:01,040


191
00:11:55,700 --> 00:12:01,040
normal continuity debug okay

