1
00:00:00,060 --> 00:00:08,519
another important part of automation is

2
00:00:04,380 --> 00:00:10,320
batch scripts so by using matches

3
00:00:08,519 --> 00:00:14,900
scripts we could automate tasks and

4
00:00:10,320 --> 00:00:18,119
tasks in HyperDbg and batches groups are

5
00:00:14,900 --> 00:00:21,180
it's like a simple batch scripts that

6
00:00:18,119 --> 00:00:23,820
use sharp sign as the command character

7
00:00:21,180 --> 00:00:26,100
and but this is a little bit different

8
00:00:23,820 --> 00:00:29,460
in the script engine a script engine it

9
00:00:26,100 --> 00:00:31,920
has CLI commenting style so if you want

10
00:00:29,460 --> 00:00:34,140
to make comments in batch scripts you

11
00:00:31,920 --> 00:00:38,100
could use this sharp sign while if you

12
00:00:34,140 --> 00:00:41,820
want to create a comments in a script

13
00:00:38,100 --> 00:00:44,399
engine in s grips then you should use a

14
00:00:41,820 --> 00:00:49,379
sea like commenting style the extensions

15
00:00:44,399 --> 00:00:53,280
for HyperDbg batchescript is .ds

16
00:00:49,379 --> 00:00:58,680
which stands for debugger script and

17
00:00:53,280 --> 00:01:02,100
could use this I use dispatch commands

18
00:00:58,680 --> 00:01:05,460
both use the S script engine codes in

19
00:01:02,100 --> 00:01:10,020
the batch files and the difference here

20
00:01:05,460 --> 00:01:12,960
between the script files and the batch

21
00:01:10,020 --> 00:01:16,860
files is that script files are running

22
00:01:12,960 --> 00:01:20,280
like all of them or the script engine or

23
00:01:16,860 --> 00:01:24,119
the script codes are translated to a

24
00:01:20,280 --> 00:01:26,939
standard IR format and finally it is

25
00:01:24,119 --> 00:01:30,600
passed to executed to the kernel mode

26
00:01:26,939 --> 00:01:33,299
but one thing to consider is that these

27
00:01:30,600 --> 00:01:37,740
scripts or dispatch scripts all of them

28
00:01:33,299 --> 00:01:41,820
are parsed in the user mode part of the

29
00:01:37,740 --> 00:01:44,759
debugger if you use a s script or a high

30
00:01:41,820 --> 00:01:48,000
produce group for for example for an

31
00:01:44,759 --> 00:01:50,159
event then the script itself will be

32
00:01:48,000 --> 00:01:53,520
parsed and will be converted to an IR

33
00:01:50,159 --> 00:01:56,040
format 100 to be executed in the kernel

34
00:01:53,520 --> 00:01:58,979
mode so you should just keep it keep

35
00:01:56,040 --> 00:02:02,340
this in mind and there is a command in

36
00:01:58,979 --> 00:02:06,840
HyperDbg taught a script command which

37
00:02:02,340 --> 00:02:10,080
gets its input it gets passed or a

38
00:02:06,840 --> 00:02:12,840
script file and you could use it you

39
00:02:10,080 --> 00:02:15,959
could use it to simply run some batch

40
00:02:12,840 --> 00:02:18,420
scripts and also if if you just don't

41
00:02:15,959 --> 00:02:21,239
want to open the hybrid widget directly

42
00:02:18,420 --> 00:02:25,080
then there are also other Arguments for

43
00:02:21,239 --> 00:02:27,080
the hyperactivity executable file hyper

44
00:02:25,080 --> 00:02:30,660
[pause]

45
00:02:27,080 --> 00:02:33,000
bg-cli.exe you pass dash dash script

46
00:02:30,660 --> 00:02:35,940
parameter to the command line then you

47
00:02:33,000 --> 00:02:37,920
could also use the executable file

48
00:02:35,940 --> 00:02:40,440
directly for example imagine that we

49
00:02:37,920 --> 00:02:44,099
have this simple Azure script which

50
00:02:40,440 --> 00:02:49,580
simply connects to the local debugger in

51
00:02:44,099 --> 00:02:52,860
VMI mode then it opens a log file

52
00:02:49,580 --> 00:02:55,459
tries to intercept system calls for this

53
00:02:52,860 --> 00:02:58,980
specific process

54
00:02:55,459 --> 00:03:02,340
after sometimes then it clears the

55
00:02:58,980 --> 00:03:06,300
events and unloads the debugger remove

56
00:03:02,340 --> 00:03:09,060
the driver finally closes the lock and

57
00:03:06,300 --> 00:03:12,000
exit if you want to directly run this

58
00:03:09,060 --> 00:03:14,959
script we could use the following

59
00:03:12,000 --> 00:03:18,239
command lines directly hyper

60
00:03:14,959 --> 00:03:21,540
dvg.co.exe dash dash s script and we

61
00:03:18,239 --> 00:03:24,659
will pass the file address

62
00:03:21,540 --> 00:03:28,440
it's also possible to pass some

63
00:03:24,659 --> 00:03:31,200
arguments to the script engine or to the

64
00:03:28,440 --> 00:03:34,440
to to the scripts or to the batches

65
00:03:31,200 --> 00:03:36,599
scripts which can be used used in both

66
00:03:34,440 --> 00:03:40,440
Bachelor scripts and script engine

67
00:03:36,599 --> 00:03:44,640
the format for the arguments is dollar

68
00:03:40,440 --> 00:03:48,659
sign Arc zero so it's somehow like sudo

69
00:03:44,640 --> 00:03:51,540
registers so the arguments are also

70
00:03:48,659 --> 00:03:56,220
treated as to the registers the first

71
00:03:51,540 --> 00:03:59,040
argument exactly like conventional C

72
00:03:56,220 --> 00:04:01,879
 codes or conventional main in

73
00:03:59,040 --> 00:04:07,019
programming languages the first argument

74
00:04:01,879 --> 00:04:11,220
contains the path to the .ds file and

75
00:04:07,019 --> 00:04:13,879
arguments can be both an expression a

76
00:04:11,220 --> 00:04:18,180
constant or a district

77
00:04:13,879 --> 00:04:21,840
so comma all of the constants are still

78
00:04:18,180 --> 00:04:25,259
considered in hex format so if you just

79
00:04:21,840 --> 00:04:28,979
for example provide 10 as the

80
00:04:25,259 --> 00:04:32,759
argument and it's treated as 0x and 10

81
00:04:28,979 --> 00:04:35,960
stories in a hexadecimal format and here

82
00:04:32,759 --> 00:04:40,500
is an example of how we can pass

83
00:04:35,960 --> 00:04:43,620
arguments in both directly by using the

84
00:04:40,500 --> 00:04:45,780
executable file or by using a data

85
00:04:43,620 --> 00:04:50,880
script command I think it's pretty clear

86
00:04:45,780 --> 00:04:52,820
in this example we passed 0x55 in

87
00:04:50,880 --> 00:04:57,479
hexadecimal format and the second

88
00:04:52,820 --> 00:05:01,620
argument is rx register plus RB is

89
00:04:57,479 --> 00:05:05,220
register the third argument is one two

90
00:05:01,620 --> 00:05:08,780
three in hex format and the same is also

91
00:05:05,220 --> 00:05:08,780
true about the description

