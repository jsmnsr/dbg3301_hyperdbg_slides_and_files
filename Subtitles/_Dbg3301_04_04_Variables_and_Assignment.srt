1
00:00:00,000 --> 00:00:08,519
now let's see how we assign variables

2
00:00:04,860 --> 00:00:11,820
and how variables and assignments work

3
00:00:08,519 --> 00:00:13,860
in HyperDbg variables or differently

4
00:00:11,820 --> 00:00:16,800
and an essential part of storing

5
00:00:13,860 --> 00:00:21,240
registers value results of expressions

6
00:00:16,800 --> 00:00:25,199
or anything else in the description and

7
00:00:21,240 --> 00:00:27,180
when it comes to a multi-core scenario

8
00:00:25,199 --> 00:00:29,939
everything might be accessed

9
00:00:27,180 --> 00:00:34,079
simultaneously in different course so

10
00:00:29,939 --> 00:00:37,020
that's why we need to have some local

11
00:00:34,079 --> 00:00:39,719
variables for each call the context of

12
00:00:37,020 --> 00:00:42,540
the local variables are based on each

13
00:00:39,719 --> 00:00:44,940
core and the thing is the important

14
00:00:42,540 --> 00:00:47,160
thing is that in the current version of

15
00:00:44,940 --> 00:00:49,739
the script engine hyper hardware hybrid

16
00:00:47,160 --> 00:00:53,700
widget doesn't having types so

17
00:00:49,739 --> 00:00:57,780
everything is considered as a u in 64 or

18
00:00:53,700 --> 00:01:03,840
a 64-bit integer value Gerber variables

19
00:00:57,780 --> 00:01:06,900
are start with a but prefix a bit before

20
00:01:03,840 --> 00:01:10,080
the name of the value but if you don't

21
00:01:06,900 --> 00:01:12,060
specify any prefix before the name of

22
00:01:10,080 --> 00:01:14,159
the value then it's considered as a

23
00:01:12,060 --> 00:01:16,860
local variable for example the first

24
00:01:14,159 --> 00:01:19,380
example it just assigns the my local

25
00:01:16,860 --> 00:01:22,979
variable to one which is a local

26
00:01:19,380 --> 00:01:26,280
variable and the second one as it starts

27
00:01:22,979 --> 00:01:28,799
with the dots it just assigns two to the

28
00:01:26,280 --> 00:01:30,600
my global variable as I mentioned these

29
00:01:28,799 --> 00:01:33,540
four local variables are accessible

30
00:01:30,600 --> 00:01:36,900
through the course but global variables

31
00:01:33,540 --> 00:01:40,079
are available through all of the course

32
00:01:36,900 --> 00:01:42,900
there's some memory sharing techniques

33
00:01:40,079 --> 00:01:45,600
used in hybrid which because it's very

34
00:01:42,900 --> 00:01:48,240
clear that it's a it's some kind of

35
00:01:45,600 --> 00:01:51,299
operating system issues that you cannot

36
00:01:48,240 --> 00:01:54,479
access a global variable simultaneously

37
00:01:51,299 --> 00:01:57,000
between different cores we have some

38
00:01:54,479 --> 00:02:00,020
intellect or screen lock functions that

39
00:01:57,000 --> 00:02:03,780
you can use to access a global variable

40
00:02:00,020 --> 00:02:06,360
and share a variable between these

41
00:02:03,780 --> 00:02:09,360
scores will truly discuss about it

42
00:02:06,360 --> 00:02:12,180
later this is the an example of L value

43
00:02:09,360 --> 00:02:15,239
register assignment we saw some examples

44
00:02:12,180 --> 00:02:19,140
from it in the previous slides but 

45
00:02:15,239 --> 00:02:22,560
here's a complete explanation here's how

46
00:02:19,140 --> 00:02:26,360
we can change the value of each

47
00:02:22,560 --> 00:02:31,099
registers like we I use an at sign

48
00:02:26,360 --> 00:02:33,660
Rex which is equal to 0x55 which

49
00:02:31,099 --> 00:02:36,120
directly changes the RX value times

50
00:02:33,660 --> 00:02:42,319
static to aesthetic value or if you want

51
00:02:36,120 --> 00:02:47,280
to change cr0 variable or cr0

52
00:02:42,319 --> 00:02:51,780
register value to assign to cr0 which is

53
00:02:47,280 --> 00:02:55,800
 word with 0x4 if we want to set the

54
00:02:51,780 --> 00:02:58,920
third bit of this cr0 then we use 

55
00:02:55,800 --> 00:03:02,040
this second command or if we want to

56
00:02:58,920 --> 00:03:04,920
change the zero flag or set or onset the

57
00:03:02,040 --> 00:03:09,540
zero flag we'll use it like this if we

58
00:03:04,920 --> 00:03:13,080
want to just set it to if we want to

59
00:03:09,540 --> 00:03:16,080
set the zero flag and we assign it to

60
00:03:13,080 --> 00:03:18,599
one or if we want to answer that zero

61
00:03:16,080 --> 00:03:22,140
Effect then we assign it to zero also

62
00:03:18,599 --> 00:03:26,700
it's possible to use a simple 

63
00:03:22,140 --> 00:03:29,120
expression and assign a register to an

64
00:03:26,700 --> 00:03:29,120
expression

