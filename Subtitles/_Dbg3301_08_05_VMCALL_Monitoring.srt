1
00:00:00,000 --> 00:00:05,279
there is also another possible scenario

2
00:00:03,120 --> 00:00:06,919
if you want to monitor in the nested

3
00:00:05,279 --> 00:00:09,720
virtualization environment because

4
00:00:06,919 --> 00:00:12,719
hybridizure is the first driver in the

5
00:00:09,720 --> 00:00:14,880
stack layer and it receives the VM call

6
00:00:12,719 --> 00:00:17,460
and we could decide whether we want to

7
00:00:14,880 --> 00:00:20,520
pass it to the top level hypervisor or

8
00:00:17,460 --> 00:00:24,119
we could we want to modify it we could

9
00:00:20,520 --> 00:00:27,599
make them make change on them by

10
00:00:24,119 --> 00:00:31,439
using the VM call command this is

11
00:00:27,599 --> 00:00:36,719
actually an event that is exported in

12
00:00:31,439 --> 00:00:39,780
HyperDbg so every execution of a VM

13
00:00:36,719 --> 00:00:44,280
call instruction in the targets targets

14
00:00:39,780 --> 00:00:47,040
virtual machine will trigger 

15
00:00:44,280 --> 00:00:49,800
HyperDbg call command and it's

16
00:00:47,040 --> 00:00:52,020
also up to you up to you to whether to

17
00:00:49,800 --> 00:00:55,320
pass it to the top level hypervisor or

18
00:00:52,020 --> 00:00:57,239
ignore it for example if you want to

19
00:00:55,320 --> 00:01:00,379
Mentor some of the hyper calls in

20
00:00:57,239 --> 00:01:05,820
hyper-v you could use this command

21
00:01:00,379 --> 00:01:09,180
and you can also suppress the request so

22
00:01:05,820 --> 00:01:12,780
here is an example for example this VM

23
00:01:09,180 --> 00:01:15,619
call is executed and we show the

24
00:01:12,780 --> 00:01:15,619
parameters here

